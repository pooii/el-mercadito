/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import Persistencia.ControladoraPersistencia;
import Persistencia.exceptions.NonexistentEntityException;
import Persistencia.exceptions.PreexistingEntityException;
import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import model.Empleado;

/**
 *
 * @author tonio
 */
public class EmpleadoController implements Serializable {
    
    public static ControladoraPersistencia getPersistencia() {
        return persistencia;
    }
    
    private List<Empleado> listaEmpleados = new LinkedList();
    private static final ControladoraPersistencia persistencia = new ControladoraPersistencia();

    public EmpleadoController() {
        this.listaEmpleados = new LinkedList();
    }
    public List<Empleado> getListaEmpleados() {
        return listaEmpleados;
    }

    public void setListaEmpleados(List<Empleado> listaEmpleados) {
        this.listaEmpleados = listaEmpleados;
    }
    public List<Empleado> cargarListaEmpleadosBD() {
        return this.listaEmpleados = EmpleadoController.persistencia.BuscarListaEmpleadoPersis();
    }
    
    
    //EMPLEADO
    public void ModificarUnEmpleado(Empleado unEmpleado, String idPersona, String apellido, String nombre, String fechaNac, String sexo, String estadoCivil, String fechaIngreso, String cargo, String telefono, String direccion, String localidad, String provincia) throws PreexistingEntityException, Exception{
        unEmpleado.setIdPersona(idPersona);
        unEmpleado.setApellido(apellido);
        unEmpleado.setNombre(nombre);
        unEmpleado.setFechaNac(fechaNac);
        unEmpleado.setSexo(sexo);
        unEmpleado.setEstadoCivil(estadoCivil);
        unEmpleado.setFechaIngreso(fechaIngreso);
        unEmpleado.setCargo(cargo);
        unEmpleado.setTelefono(telefono);
        unEmpleado.setDireccion(direccion);
        unEmpleado.setProvincia(provincia);
        unEmpleado.setLocalidad(localidad);
        EmpleadoController.persistencia.ModificarUnEmpleadoPersis(unEmpleado);
    }    
    public void AgregarUnEmpleado(String idPersona, String apellido, String nombre, String fechaNac, String sexo, String estadoCivil, String fechaIngreso, String cargo, String telefono, String direccion, String localidad, String provincia) throws PreexistingEntityException, Exception{
	Empleado unEmpleado = new Empleado(idPersona, apellido, nombre, fechaNac, sexo, estadoCivil, fechaIngreso, cargo, telefono, direccion, localidad, provincia);
	this.listaEmpleados.add(unEmpleado);
	EmpleadoController.persistencia.AgregarUnEmpleadoPersis(unEmpleado);    
    }
    public Empleado BuscarEmpleado(String idPersona){
        Empleado aux=null, emp;
        Iterator it = this.cargarListaEmpleadosBD().iterator();
        while(it.hasNext()){
            emp=(Empleado) it.next();
            if(emp.getIdPersona().equals(idPersona)){
                aux=emp;
            }
        }
        return aux;
    }
    public void BorrarEmpleado(String idPersona, String apellido, String nombre, String fechaNac, String sexo, String estadoCivil, String fechaIngreso, String cargo, String telefono, String direccion, String localidad, String provincia) throws PreexistingEntityException, Exception{
     Empleado unEmpleado = new Empleado(idPersona, apellido, nombre, fechaNac, sexo, estadoCivil, fechaIngreso, cargo, telefono, direccion, localidad, provincia);
     this.listaEmpleados.remove(unEmpleado);
    EmpleadoController.persistencia.BorrarEmpleadoPersis(unEmpleado.getIdPersona());
    }
    
}
