/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package view;

import ElMercadito.ElMercadito;

public class JFramePrincipal extends javax.swing.JFrame {
    private ElMercadito unMercadito;
    public JFramePrincipal(ElMercadito unMercadito) {
        this.unMercadito=unMercadito;             
        this.setVisible(true);
        initComponents();
        this.setExtendedState(MAXIMIZED_BOTH);
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSplitPane1 = new javax.swing.JSplitPane();
        jPanel1 = new javax.swing.JPanel();
        btnFinanza = new javax.swing.JButton();
        btnTranferencia = new javax.swing.JButton();
        btnCompra = new javax.swing.JButton();
        btnVenta = new javax.swing.JButton();
        btnAdministracion = new javax.swing.JButton();
        escritorio = new javax.swing.JDesktopPane();
        jMenuBarPrincipal = new javax.swing.JMenuBar();
        menuArchivo = new javax.swing.JMenu();
        jMenuItemModifUsuario = new javax.swing.JMenuItem();
        jMenuItemCerrarSesion = new javax.swing.JMenuItem();
        jMenuItemSalir = new javax.swing.JMenuItem();
        menuAdministrador = new javax.swing.JMenu();
        jMenuItem4 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("El Mercadito");

        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        btnFinanza.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnFinanza.setIcon(new javax.swing.ImageIcon(getClass().getResource("/view/images/finanza2.png"))); // NOI18N
        btnFinanza.setText("Finanzas");
        btnFinanza.setBorder(null);
        btnFinanza.setBorderPainted(false);
        btnFinanza.setContentAreaFilled(false);
        btnFinanza.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnFinanza.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnFinanza.setIconTextGap(2);
        btnFinanza.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/view/images/finanza3.png"))); // NOI18N
        btnFinanza.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/view/images/finanza1.png"))); // NOI18N
        btnFinanza.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnFinanza.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnFinanza.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFinanzaActionPerformed(evt);
            }
        });

        btnTranferencia.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnTranferencia.setIcon(new javax.swing.ImageIcon(getClass().getResource("/view/images/tranferir2.png"))); // NOI18N
        btnTranferencia.setText("Tranferencias");
        btnTranferencia.setBorder(null);
        btnTranferencia.setBorderPainted(false);
        btnTranferencia.setContentAreaFilled(false);
        btnTranferencia.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnTranferencia.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnTranferencia.setIconTextGap(2);
        btnTranferencia.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/view/images/tranferir3.png"))); // NOI18N
        btnTranferencia.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/view/images/tranferir1.png"))); // NOI18N
        btnTranferencia.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnTranferencia.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnTranferencia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTranferenciaActionPerformed(evt);
            }
        });

        btnCompra.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnCompra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/view/images/comprar2.png"))); // NOI18N
        btnCompra.setText("Compras");
        btnCompra.setBorder(null);
        btnCompra.setBorderPainted(false);
        btnCompra.setContentAreaFilled(false);
        btnCompra.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnCompra.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnCompra.setIconTextGap(2);
        btnCompra.setMaximumSize(new java.awt.Dimension(91, 97));
        btnCompra.setMinimumSize(new java.awt.Dimension(91, 97));
        btnCompra.setPreferredSize(new java.awt.Dimension(91, 97));
        btnCompra.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/view/images/comprar3.png"))); // NOI18N
        btnCompra.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/view/images/comprar1.png"))); // NOI18N
        btnCompra.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnCompra.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnCompra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCompraActionPerformed(evt);
            }
        });

        btnVenta.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnVenta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/view/images/vender2.png"))); // NOI18N
        btnVenta.setText("Ventas");
        btnVenta.setBorder(null);
        btnVenta.setBorderPainted(false);
        btnVenta.setContentAreaFilled(false);
        btnVenta.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnVenta.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnVenta.setIconTextGap(2);
        btnVenta.setMaximumSize(new java.awt.Dimension(91, 97));
        btnVenta.setMinimumSize(new java.awt.Dimension(91, 97));
        btnVenta.setPreferredSize(new java.awt.Dimension(91, 97));
        btnVenta.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/view/images/vender3.png"))); // NOI18N
        btnVenta.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/view/images/vender1.png"))); // NOI18N
        btnVenta.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnVenta.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVentaActionPerformed(evt);
            }
        });

        btnAdministracion.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnAdministracion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/view/images/administrar2.png"))); // NOI18N
        btnAdministracion.setText("Administración");
        btnAdministracion.setBorder(null);
        btnAdministracion.setBorderPainted(false);
        btnAdministracion.setContentAreaFilled(false);
        btnAdministracion.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAdministracion.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnAdministracion.setIconTextGap(2);
        btnAdministracion.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/view/images/administrar3.png"))); // NOI18N
        btnAdministracion.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/view/images/administrar1.png"))); // NOI18N
        btnAdministracion.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnAdministracion.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnAdministracion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAdministracionActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnFinanza, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnTranferencia, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnVenta, javax.swing.GroupLayout.DEFAULT_SIZE, 112, Short.MAX_VALUE)
                    .addComponent(btnAdministracion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnCompra, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(45, 45, 45)
                .addComponent(btnAdministracion, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCompra, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnTranferencia, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnFinanza, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout escritorioLayout = new javax.swing.GroupLayout(escritorio);
        escritorio.setLayout(escritorioLayout);
        escritorioLayout.setHorizontalGroup(
            escritorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 739, Short.MAX_VALUE)
        );
        escritorioLayout.setVerticalGroup(
            escritorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 604, Short.MAX_VALUE)
        );

        jMenuBarPrincipal.setBackground(new java.awt.Color(204, 204, 255));
        jMenuBarPrincipal.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        menuArchivo.setText("Archivo");
        menuArchivo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuArchivoActionPerformed(evt);
            }
        });

        jMenuItemModifUsuario.setText("Modificar mi usuario");
        jMenuItemModifUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemModifUsuarioActionPerformed(evt);
            }
        });
        menuArchivo.add(jMenuItemModifUsuario);

        jMenuItemCerrarSesion.setText("Cerrar sesión");
        jMenuItemCerrarSesion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemCerrarSesionActionPerformed(evt);
            }
        });
        menuArchivo.add(jMenuItemCerrarSesion);

        jMenuItemSalir.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemSalir.setText("Salir");
        jMenuItemSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemSalirActionPerformed(evt);
            }
        });
        menuArchivo.add(jMenuItemSalir);

        jMenuBarPrincipal.add(menuArchivo);

        menuAdministrador.setText("Administrador");

        jMenuItem4.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem4.setText("Panel de usuarios");
        menuAdministrador.add(jMenuItem4);

        jMenuBarPrincipal.add(menuAdministrador);

        setJMenuBar(jMenuBarPrincipal);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(escritorio)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(escritorio)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItemModifUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemModifUsuarioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItemModifUsuarioActionPerformed

    private void jMenuItemCerrarSesionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemCerrarSesionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItemCerrarSesionActionPerformed

    private void jMenuItemSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemSalirActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_jMenuItemSalirActionPerformed

    private void btnVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVentaActionPerformed
        // TODO add your handling code here:
       InternalFrameCaja ventanaCaja = new InternalFrameCaja(this.unMercadito);
       escritorio.add(ventanaCaja);
       ventanaCaja.show();
    }//GEN-LAST:event_btnVentaActionPerformed

    private void btnFinanzaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFinanzaActionPerformed
        InternalFrameFinanzas ventanaFinanza = new InternalFrameFinanzas(this.unMercadito);
        escritorio.add(ventanaFinanza);
        ventanaFinanza.show();
    }//GEN-LAST:event_btnFinanzaActionPerformed

    private void btnCompraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCompraActionPerformed
       InternalFrameCompra ventanaCompra = new InternalFrameCompra(this.unMercadito);
       escritorio.add(ventanaCompra);
       ventanaCompra.show();
    }//GEN-LAST:event_btnCompraActionPerformed

    private void menuArchivoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuArchivoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_menuArchivoActionPerformed

    private void btnAdministracionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAdministracionActionPerformed
        // TODO add your handling code here:
       InternalFrameAdministracion ventanaAdministracion = new InternalFrameAdministracion(this.unMercadito);
       escritorio.add(ventanaAdministracion);
       ventanaAdministracion.show();
    }//GEN-LAST:event_btnAdministracionActionPerformed

    private void btnTranferenciaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTranferenciaActionPerformed
        InternalFrameTranferencia ventanaTranferencia = new InternalFrameTranferencia(this.unMercadito);
        escritorio.add(ventanaTranferencia);
        ventanaTranferencia.show();
    }//GEN-LAST:event_btnTranferenciaActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdministracion;
    private javax.swing.JButton btnCompra;
    private javax.swing.JButton btnFinanza;
    private javax.swing.JButton btnTranferencia;
    private javax.swing.JButton btnVenta;
    public static javax.swing.JDesktopPane escritorio;
    private javax.swing.JMenuBar jMenuBarPrincipal;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItemCerrarSesion;
    private javax.swing.JMenuItem jMenuItemModifUsuario;
    private javax.swing.JMenuItem jMenuItemSalir;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JMenu menuAdministrador;
    private javax.swing.JMenu menuArchivo;
    // End of variables declaration//GEN-END:variables
}
