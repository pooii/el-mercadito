/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package view;

import ElMercadito.ElMercadito;
import Persistencia.exceptions.NonexistentEntityException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import model.Deposito;
import model.DescripcionArticulo;
import model.Empleado;
import model.Gondola;
import model.Proveedor;
//import org.apache.poi.hssf.record.formula.functions.False;

public class InternalFrameCompra extends javax.swing.JInternalFrame {
    private ElMercadito unMercadito;
    private DefaultTableModel listaOrdenCompra = new DefaultTableModel();
    private int contador =0;
    private float total =0;
    /**
     * Creates new form InternalFrameAdministracion
     * @param unMercadito
     */
    public InternalFrameCompra(ElMercadito unMercadito) {
        this.unMercadito= unMercadito;
        initComponents();        
        btnActualizarStock.setEnabled(false);
        txtCantArt.setEditable(false);
        listaOrdenCompra.addColumn("#");
        listaOrdenCompra.addColumn("Codigo");
        listaOrdenCompra.addColumn("Descripción");
        listaOrdenCompra.addColumn("Cantidad");
        listaOrdenCompra.addColumn("$ Unitario");
        listaOrdenCompra.addColumn("Sub Total");
        
        this.tableOrdenCompra.setModel(listaOrdenCompra);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTextField1 = new javax.swing.JTextField();
        jScrollBar1 = new javax.swing.JScrollBar();
        jTabbedPCompra = new javax.swing.JTabbedPane();
        jPanelIngComprobante = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jLabel29 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        txtNumFactura = new javax.swing.JTextField();
        txtFechaFactura = new com.toedter.calendar.JDateChooser();
        jLabel32 = new javax.swing.JLabel();
        txtTotalFactura = new javax.swing.JTextField();
        jLabel33 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jLabel31 = new javax.swing.JLabel();
        txtCuitFactura = new javax.swing.JTextField();
        jLabel34 = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        txtRazonSocialFactura = new javax.swing.JTextField();
        txtCondIvaFactura = new javax.swing.JTextField();
        btnBuscarProveedorFactura = new javax.swing.JButton();
        txtCambiarProveedorFactura = new javax.swing.JButton();
        btnGuardarFactura = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnNuevo = new javax.swing.JButton();
        txtTipoCompra = new javax.swing.JTextField();
        jPanelGeneraComprobante = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        btnBuscarProveedor = new javax.swing.JButton();
        txtNombreProveedor = new javax.swing.JTextField();
        txtTerminoEntrega = new javax.swing.JTextField();
        txtDireccionEntrega = new javax.swing.JTextField();
        lblProveedor = new javax.swing.JLabel();
        lblFechaPedido = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        txtBuscarProveedorOrdenCompra = new javax.swing.JTextField();
        jPanel6 = new javax.swing.JPanel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        txtCodArticuloOrdenCompra = new javax.swing.JTextField();
        txtCantArticuloOrdenCompra = new javax.swing.JTextField();
        btnAgregarArticulo = new javax.swing.JButton();
        btnCancelarAgregarArticulo = new javax.swing.JButton();
        fechaPedido = new com.toedter.calendar.JDateChooser();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableOrdenCompra = new javax.swing.JTable();
        btnBuscarEmpleado = new javax.swing.JButton();
        txtBuscarEmpleado = new javax.swing.JTextField();
        jLabel24 = new javax.swing.JLabel();
        txtNombreEmpleado = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        txtCostoTotalOrdenCompra = new javax.swing.JTextField();
        btnNuevaOrdenCompra = new javax.swing.JButton();
        btnRegistrarOrden = new javax.swing.JButton();
        btnCancelarOrden = new javax.swing.JButton();
        btnImprimir = new javax.swing.JButton();
        btnEliminarFila = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtBuscarArt = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtCodBarrasArt = new javax.swing.JTextField();
        txtNombreArt = new javax.swing.JTextField();
        txtDescripcionArt = new javax.swing.JTextField();
        txtTipoEnvaseArt = new javax.swing.JTextField();
        txtUnidadMedidaArt = new javax.swing.JTextField();
        txtCantUnMedidaArt = new javax.swing.JTextField();
        txtPrecioCompraArt = new javax.swing.JTextField();
        txtPrecioVtaArt = new javax.swing.JTextField();
        txtPrecioVtaMayArt = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        txtStockActualArt = new javax.swing.JTextField();
        jLabel27 = new javax.swing.JLabel();
        txtCantMinima = new javax.swing.JTextField();
        jLabel28 = new javax.swing.JLabel();
        txtUnidadesPorBulto = new javax.swing.JTextField();
        btnBuscarArt = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        txtCantArt = new javax.swing.JTextField();
        jPanel9 = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        txtCuitProvActualizar = new javax.swing.JTextField();
        txtRazonSocialActualizar = new javax.swing.JTextField();
        btnBuscarProveedorArt = new javax.swing.JButton();
        btnCambiarProveedor = new javax.swing.JButton();
        jLabel16 = new javax.swing.JLabel();
        txtStockActualDepActualizar = new javax.swing.JTextField();
        btnActualizarStock = new javax.swing.JButton();
        jLabel13 = new javax.swing.JLabel();
        txtStockReposicionArt = new javax.swing.JTextField();
        chkCambiarRepo = new javax.swing.JCheckBox();
        jLabel12 = new javax.swing.JLabel();
        txtStockMinimoArt = new javax.swing.JTextField();
        chkCambiarMin = new javax.swing.JCheckBox();

        jTextField1.setText("jTextField1");

        setClosable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Compras");
        setPreferredSize(new java.awt.Dimension(767, 657));

        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true), "Registrar Factura de Compra", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 11))); // NOI18N

        jLabel29.setText("Número Factura:");

        jLabel30.setText("Fecha:");

        jLabel32.setText("Total Factura: ($ARS)");

        jLabel33.setText("Condiciones de Venta:");

        jPanel8.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true), "Proveedor", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 11))); // NOI18N

        jLabel31.setText("Nro. CUIT:");

        jLabel34.setText("Razón Social:");

        jLabel35.setText("Cond. Iva:");

        txtRazonSocialFactura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtRazonSocialFacturaActionPerformed(evt);
            }
        });

        txtCondIvaFactura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCondIvaFacturaActionPerformed(evt);
            }
        });

        btnBuscarProveedorFactura.setText("Buscar");
        btnBuscarProveedorFactura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarProveedorFacturaActionPerformed(evt);
            }
        });

        txtCambiarProveedorFactura.setText("Cambiar");
        txtCambiarProveedorFactura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCambiarProveedorFacturaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addComponent(jLabel31)
                        .addGap(18, 18, 18)
                        .addComponent(txtCuitFactura, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addComponent(jLabel34)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtRazonSocialFactura))
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addComponent(jLabel35)
                        .addGap(18, 18, 18)
                        .addComponent(txtCondIvaFactura)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 41, Short.MAX_VALUE)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnBuscarProveedorFactura)
                    .addComponent(txtCambiarProveedorFactura))
                .addGap(35, 35, 35))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel31)
                    .addComponent(txtCuitFactura, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBuscarProveedorFactura))
                .addGap(18, 18, 18)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel34)
                    .addComponent(txtRazonSocialFactura, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCambiarProveedorFactura))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel35)
                    .addComponent(txtCondIvaFactura, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(18, Short.MAX_VALUE))
        );

        btnGuardarFactura.setText("Guardar");
        btnGuardarFactura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarFacturaActionPerformed(evt);
            }
        });

        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        btnNuevo.setText("Nuevo");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(jLabel29)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtNumFactura, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(29, 29, 29)
                        .addComponent(jLabel30)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtFechaFactura, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(167, 167, 167))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addComponent(btnNuevo)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnGuardarFactura)
                                .addGap(18, 18, 18)
                                .addComponent(btnCancelar))
                            .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addComponent(jLabel32)
                                .addGap(18, 18, 18)
                                .addComponent(txtTotalFactura, javax.swing.GroupLayout.DEFAULT_SIZE, 107, Short.MAX_VALUE))
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addComponent(jLabel33)
                                .addGap(18, 18, 18)
                                .addComponent(txtTipoCompra)))
                        .addGap(35, 35, 35))))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtFechaFactura, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel29)
                        .addComponent(jLabel30)
                        .addComponent(txtNumFactura, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel32)
                            .addComponent(txtTotalFactura, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel33)
                            .addComponent(txtTipoCompra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(42, 42, 42)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnGuardarFactura)
                    .addComponent(btnCancelar)
                    .addComponent(btnNuevo))
                .addContainerGap(26, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanelIngComprobanteLayout = new javax.swing.GroupLayout(jPanelIngComprobante);
        jPanelIngComprobante.setLayout(jPanelIngComprobanteLayout);
        jPanelIngComprobanteLayout.setHorizontalGroup(
            jPanelIngComprobanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelIngComprobanteLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanelIngComprobanteLayout.setVerticalGroup(
            jPanelIngComprobanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelIngComprobanteLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(288, Short.MAX_VALUE))
        );

        jTabbedPCompra.addTab("Ingreso de Comprobantes", jPanelIngComprobante);

        jLabel18.setFont(new java.awt.Font("Traditional Arabic", 1, 24)); // NOI18N
        jLabel18.setText("El Mercadito S.A.");

        jLabel19.setText("Sucursal Posadas - Misiones");

        jLabel26.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel26.setForeground(new java.awt.Color(153, 0, 0));
        jLabel26.setText("ORDEN DE COMPRA");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(264, 264, 264)
                        .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(292, 292, 292)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(jLabel26))
                            .addComponent(jLabel19))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(jLabel18)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel19)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel26)
                .addContainerGap(14, Short.MAX_VALUE))
        );

        jPanel5.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));

        btnBuscarProveedor.setText("Buscar Proveedor");
        btnBuscarProveedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarProveedorActionPerformed(evt);
            }
        });

        lblProveedor.setText("Proveedor:");

        lblFechaPedido.setText("Fecha del Pedido:");

        jLabel20.setText("Término de Entrega:");

        jLabel21.setText("Dirección de Entrega:");

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true), "Artículos", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 11))); // NOI18N

        jLabel22.setText("Cod. Artículo:");

        jLabel23.setText("Cantidad:");

        txtCantArticuloOrdenCompra.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCantArticuloOrdenCompraKeyTyped(evt);
            }
        });

        btnAgregarArticulo.setText("Agregar");
        btnAgregarArticulo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarArticuloActionPerformed(evt);
            }
        });

        btnCancelarAgregarArticulo.setText("Cancelar");
        btnCancelarAgregarArticulo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarAgregarArticuloActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel22)
                    .addComponent(jLabel23))
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtCodArticuloOrdenCompra)
                    .addComponent(txtCantArticuloOrdenCompra))
                .addContainerGap())
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addComponent(btnAgregarArticulo)
                .addGap(18, 18, 18)
                .addComponent(btnCancelarAgregarArticulo)
                .addContainerGap(52, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel22)
                    .addComponent(txtCodArticuloOrdenCompra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel23)
                    .addComponent(txtCantArticuloOrdenCompra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAgregarArticulo)
                    .addComponent(btnCancelarAgregarArticulo)))
        );

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel21)
                        .addGap(18, 18, 18)
                        .addComponent(txtDireccionEntrega))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(btnBuscarProveedor)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBuscarProveedorOrdenCompra))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel20)
                            .addComponent(lblFechaPedido)
                            .addComponent(lblProveedor))
                        .addGap(23, 23, 23)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(txtTerminoEntrega, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 161, Short.MAX_VALUE)
                            .addComponent(txtNombreProveedor, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 161, Short.MAX_VALUE)
                            .addComponent(fechaPedido, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addGap(152, 152, 152)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(26, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnBuscarProveedor)
                            .addComponent(txtBuscarProveedorOrdenCompra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(5, 5, 5)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtNombreProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblProveedor))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblFechaPedido)
                            .addComponent(fechaPedido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(12, 12, 12)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtTerminoEntrega, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel20))
                        .addGap(7, 7, 7)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtDireccionEntrega, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel21))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        tableOrdenCompra.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tableOrdenCompra);

        btnBuscarEmpleado.setText("Buscar Encargado");
        btnBuscarEmpleado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarEmpleadoActionPerformed(evt);
            }
        });

        jLabel24.setText("Autorizado por:");

        txtNombreEmpleado.setEditable(false);

        jLabel25.setText("Costo Total:");

        txtCostoTotalOrdenCompra.setEditable(false);

        btnNuevaOrdenCompra.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnNuevaOrdenCompra.setText("Nuevo");
        btnNuevaOrdenCompra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevaOrdenCompraActionPerformed(evt);
            }
        });

        btnRegistrarOrden.setText("Registrar");
        btnRegistrarOrden.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegistrarOrdenActionPerformed(evt);
            }
        });

        btnCancelarOrden.setText("Cancelar");
        btnCancelarOrden.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarOrdenActionPerformed(evt);
            }
        });

        btnImprimir.setText("Imprimir");
        btnImprimir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImprimirActionPerformed(evt);
            }
        });

        btnEliminarFila.setText("Eliminar Fila");
        btnEliminarFila.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarFilaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelGeneraComprobanteLayout = new javax.swing.GroupLayout(jPanelGeneraComprobante);
        jPanelGeneraComprobante.setLayout(jPanelGeneraComprobanteLayout);
        jPanelGeneraComprobanteLayout.setHorizontalGroup(
            jPanelGeneraComprobanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jScrollPane1)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelGeneraComprobanteLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelGeneraComprobanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelGeneraComprobanteLayout.createSequentialGroup()
                        .addComponent(btnBuscarEmpleado)
                        .addGap(18, 18, 18)
                        .addGroup(jPanelGeneraComprobanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtNombreEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtBuscarEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jLabel24))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 157, Short.MAX_VALUE)
                .addGroup(jPanelGeneraComprobanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelGeneraComprobanteLayout.createSequentialGroup()
                        .addGroup(jPanelGeneraComprobanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanelGeneraComprobanteLayout.createSequentialGroup()
                                .addGap(96, 96, 96)
                                .addComponent(jLabel25)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtCostoTotalOrdenCompra, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanelGeneraComprobanteLayout.createSequentialGroup()
                                .addComponent(btnEliminarFila)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnRegistrarOrden)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnImprimir)))
                        .addGap(55, 55, 55))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelGeneraComprobanteLayout.createSequentialGroup()
                        .addComponent(btnNuevaOrdenCompra)
                        .addGap(35, 35, 35)
                        .addComponent(btnCancelarOrden)
                        .addGap(76, 76, 76))))
        );
        jPanelGeneraComprobanteLayout.setVerticalGroup(
            jPanelGeneraComprobanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelGeneraComprobanteLayout.createSequentialGroup()
                .addGroup(jPanelGeneraComprobanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelGeneraComprobanteLayout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanelGeneraComprobanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnBuscarEmpleado)
                            .addComponent(txtBuscarEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelGeneraComprobanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel25)
                        .addComponent(txtCostoTotalOrdenCompra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelGeneraComprobanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel24)
                    .addComponent(txtNombreEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnEliminarFila)
                    .addComponent(btnRegistrarOrden)
                    .addComponent(btnImprimir))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelGeneraComprobanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCancelarOrden)
                    .addComponent(btnNuevaOrdenCompra))
                .addGap(0, 40, Short.MAX_VALUE))
        );

        jTabbedPCompra.addTab("Orden de Compra", jPanelGeneraComprobante);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Ingrese Código de Barras: ");

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true), "Articulo - Descripción", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel2.setText("Cod.:");

        jLabel3.setText("Artículo:");

        jLabel4.setText("Descripción:");

        jLabel5.setText("Tipo envase:");

        jLabel6.setText("Unidad de medida:");

        jLabel7.setText("Cantidad:");

        jLabel8.setText("Precio de compra (unitario en $ARS):");

        jLabel9.setText("Precio de venta (unitario en $ARS:)");

        jLabel10.setText("Precio de venta (unitario MAY en $ARS):");

        txtCodBarrasArt.setEditable(false);

        txtNombreArt.setEditable(false);

        txtDescripcionArt.setEditable(false);

        txtTipoEnvaseArt.setEditable(false);

        txtUnidadMedidaArt.setEditable(false);

        txtCantUnMedidaArt.setEditable(false);

        txtPrecioCompraArt.setEditable(false);

        txtPrecioVtaArt.setEditable(false);

        txtPrecioVtaMayArt.setEditable(false);

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel11.setText("Stock Actual (Real):");

        txtStockActualArt.setEditable(false);
        txtStockActualArt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtStockActualArtActionPerformed(evt);
            }
        });

        jLabel27.setText("Cant. Mínima:");

        txtCantMinima.setEditable(false);

        jLabel28.setText("Unidades por Bulto:");

        txtUnidadesPorBulto.setEditable(false);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                            .addComponent(jLabel2)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(txtCodBarrasArt))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                            .addComponent(jLabel3)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(txtNombreArt))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                            .addComponent(jLabel4)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(txtDescripcionArt, javax.swing.GroupLayout.PREFERRED_SIZE, 346, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtPrecioCompraArt))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtTipoEnvaseArt))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel9)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtPrecioVtaArt, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel10)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtPrecioVtaMayArt, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtUnidadMedidaArt, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel7))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel28)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtUnidadesPorBulto, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(20, 20, 20)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtCantUnMedidaArt, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel27)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtCantMinima, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel11)
                        .addGap(18, 18, 18)
                        .addComponent(txtStockActualArt, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(27, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtCodBarrasArt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtNombreArt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtDescripcionArt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6)
                    .addComponent(jLabel7)
                    .addComponent(txtTipoEnvaseArt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtUnidadMedidaArt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCantUnMedidaArt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(txtPrecioCompraArt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel28)
                    .addComponent(txtUnidadesPorBulto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(jLabel10)
                    .addComponent(txtPrecioVtaArt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPrecioVtaMayArt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel27)
                    .addComponent(txtCantMinima, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(txtStockActualArt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(14, Short.MAX_VALUE))
        );

        btnBuscarArt.setText("Buscar");
        btnBuscarArt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarArtActionPerformed(evt);
            }
        });

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true), "Actualizar en Depósito"));

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel17.setText("Cantidad de Artículos Ingresantes:");

        txtCantArt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCantArtKeyTyped(evt);
            }
        });

        jPanel9.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true), "Proveedor"));

        jLabel14.setText("Nro. CUIT:");

        jLabel15.setText("Razón Social:");

        txtRazonSocialActualizar.setEditable(false);

        btnBuscarProveedorArt.setText("Buscar");
        btnBuscarProveedorArt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarProveedorArtActionPerformed(evt);
            }
        });

        btnCambiarProveedor.setText("Cambiar");
        btnCambiarProveedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCambiarProveedorActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addComponent(jLabel14)
                        .addGap(18, 18, 18)
                        .addComponent(txtCuitProvActualizar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addComponent(jLabel15)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtRazonSocialActualizar)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 39, Short.MAX_VALUE)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnBuscarProveedorArt)
                    .addComponent(btnCambiarProveedor))
                .addGap(17, 17, 17))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(txtCuitProvActualizar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBuscarProveedorArt))
                .addGap(18, 18, 18)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(txtRazonSocialActualizar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCambiarProveedor))
                .addContainerGap(43, Short.MAX_VALUE))
        );

        jLabel16.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel16.setText("Stock Actual Depósito:");

        txtStockActualDepActualizar.setEditable(false);

        btnActualizarStock.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnActualizarStock.setText("Actualizar Stock  en Depósito");
        btnActualizarStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarStockActionPerformed(evt);
            }
        });

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel13.setText("Stock de Reposicion (Depósito):");

        txtStockReposicionArt.setEditable(false);
        txtStockReposicionArt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtStockReposicionArtKeyTyped(evt);
            }
        });

        chkCambiarRepo.setText("Cambiar");
        chkCambiarRepo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkCambiarRepoActionPerformed(evt);
            }
        });

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel12.setText("Stock Mínimo (en Góndola):");

        txtStockMinimoArt.setEditable(false);
        txtStockMinimoArt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtStockMinimoArtKeyTyped(evt);
            }
        });

        chkCambiarMin.setText("Cambiar");
        chkCambiarMin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkCambiarMinActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(59, 59, 59)
                                .addComponent(btnActualizarStock))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel17)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtCantArt, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel16)
                                .addGap(26, 26, 26)
                                .addComponent(txtStockActualDepActualizar))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel13)
                                    .addComponent(jLabel12))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addComponent(txtStockReposicionArt, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(chkCambiarRepo))
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addComponent(txtStockMinimoArt)
                                        .addGap(18, 18, 18)
                                        .addComponent(chkCambiarMin)))))))
                .addGap(18, 18, 18)
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(txtStockActualDepActualizar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(chkCambiarRepo)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel13)
                        .addComponent(txtStockReposicionArt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(txtStockMinimoArt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chkCambiarMin))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(txtCantArt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(btnActualizarStock)
                .addGap(20, 20, 20))
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtBuscarArt, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnBuscarArt))
                            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtBuscarArt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBuscarArt))
                .addGap(18, 18, 18)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPCompra.addTab("Actualizar Stock", jPanel1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPCompra)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPCompra)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnBuscarArtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarArtActionPerformed
        try{
            DescripcionArticulo unaDescripcionArticulo;
            unaDescripcionArticulo=unMercadito.getUnaSucursal().BuscarDescripcionArticulo(this.txtBuscarArt.getText());
            this.txtCodBarrasArt.setText(unaDescripcionArticulo.getCodigoBarra());
            this.txtNombreArt.setText(unaDescripcionArticulo.getNombreArticulo());
            this.txtDescripcionArt.setText(unaDescripcionArticulo.getDescripcion());
            this.txtTipoEnvaseArt.setText(unaDescripcionArticulo.getTipoEnvase());
            this.txtUnidadMedidaArt.setText(unaDescripcionArticulo.getUnidadMedida());        
            this.txtCantUnMedidaArt.setText(String.valueOf(unaDescripcionArticulo.getCantidadUnidadMedida()));
            this.txtPrecioCompraArt.setText(String.valueOf(unaDescripcionArticulo.getPrecioCompra()));
            this.txtPrecioVtaArt.setText(String.valueOf(unaDescripcionArticulo.getPrecioVenta()));
            this.txtPrecioVtaMayArt.setText(String.valueOf(unaDescripcionArticulo.getPrecioVentaMay()));
            this.txtCantMinima.setText(String.valueOf(unaDescripcionArticulo.getCantMinMay()));
            this.txtUnidadesPorBulto.setText(String.valueOf(unaDescripcionArticulo.getUnidadesPorBulto()));       
            this.txtStockActualArt.setText(String.valueOf(unaDescripcionArticulo.getStockActual()));                 
            Deposito unDeposito;
            unDeposito=unMercadito.getUnaSucursal().BuscarDeposito(this.txtBuscarArt.getText());
            this.txtStockActualDepActualizar.setText(String.valueOf(unDeposito.getStockActualDeposito()));
            this.txtStockReposicionArt.setText(String.valueOf(unDeposito.getStockReposicion()));
            this.txtRazonSocialActualizar.setText(unDeposito.getProveedor());
            btnActualizarStock.setEnabled(true);
            txtCantArt.setEditable(true);
            btnCambiarProveedor.setEnabled(false);
            btnBuscarProveedorArt.setEnabled(true);
            txtCuitProvActualizar.setEditable(true);
        }catch (Exception ex){
                 Logger.getLogger(InternalFrameCompra.class.getName()).log(Level.SEVERE, null, ex);
                 JOptionPane.showMessageDialog(this, "¡Codigo de Barras Inexistente!");
        }
    }//GEN-LAST:event_btnBuscarArtActionPerformed

    private void txtCantArtKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCantArtKeyTyped
        char c=evt.getKeyChar();          
        if(Character.isLetter(c)) { 
            getToolkit().beep();     
            evt.consume();
            JOptionPane.showMessageDialog(this, "Este campo solo permite numeros.");
        }
    }//GEN-LAST:event_txtCantArtKeyTyped

    private void btnActualizarStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarStockActionPerformed
        int confirmar;
        confirmar=JOptionPane.showConfirmDialog(this, "Esta accion actualizara el stock del Articulo seleccionado ¿Desea continuar?", "Modificar",  JOptionPane.YES_NO_OPTION);
        if(confirmar==JOptionPane.YES_OPTION){
             try {
                int stockActual = Integer.parseInt(txtStockActualDepActualizar.getText());
                int stockIngresado = Integer.parseInt(txtCantArt.getText());
                int stockActualizado = stockActual + stockIngresado;
                Deposito unDeposito = this.unMercadito.getUnaSucursal().BuscarDeposito(this.txtCodBarrasArt.getText());
                this.unMercadito.getUnaSucursal().ModificarDeposito(unDeposito, unDeposito.getNroDeposito(), unDeposito.getCodigoBarra(), Integer.parseInt(this.txtStockReposicionArt.getText()), stockActualizado, this.txtRazonSocialActualizar.getText());           
                DescripcionArticulo unaDescripcionArticulo = this.unMercadito.getUnaSucursal().BuscarDescripcionArticulo(this.txtCodBarrasArt.getText());
                this.unMercadito.getUnaSucursal().ModificarUnaDescripcionArticulo(unaDescripcionArticulo, this.txtCodBarrasArt.getText(), this.txtNombreArt.getText(), this.txtDescripcionArt.getText(), this.txtTipoEnvaseArt.getText(), this.txtUnidadMedidaArt.getText(), Float.parseFloat(this.txtCantUnMedidaArt.getText()), Float.parseFloat(this.txtPrecioCompraArt.getText()),Float.parseFloat(this.txtPrecioVtaArt.getText()), Float.parseFloat(this.txtPrecioVtaMayArt.getText()), stockActualizado, Integer.parseInt(this.txtCantMinima.getText().toString()), Integer.parseInt(this.txtUnidadesPorBulto.getText().toString()));
                if(chkCambiarRepo.isSelected()==true){
                    Gondola unaGondola = this.unMercadito.getUnaSucursal().BuscarGondola(this.txtCodBarrasArt.getText());
                    this.unMercadito.getUnaSucursal().ModificarGondola(unaGondola, unaGondola.getNroGondola(), unaGondola.getTipoGondola(), unaGondola.getCodigoBarra(), Integer.parseInt(this.txtStockMinimoArt.getText()), unaGondola.getStockActualGondola(), unaGondola.getSectorGondola());
                }
                chkCambiarMin.setSelected(false);
                chkCambiarRepo.setSelected(false);
                
                JOptionPane.showMessageDialog(this, "¡Stock Actualizado!");
                limpiarCampos();
             } catch (NonexistentEntityException ex) {
                 Logger.getLogger(InternalFrameCompra.class.getName()).log(Level.SEVERE, null, ex);
                 JOptionPane.showMessageDialog(this, "Error con la base de datos, no existe");
             } catch (Exception ex) {
                 Logger.getLogger(InternalFrameCompra.class.getName()).log(Level.SEVERE, null, ex);
                 JOptionPane.showMessageDialog(this, "Error con la base de datos");
             }
        }else{
            limpiarCampos();
        }
    }//GEN-LAST:event_btnActualizarStockActionPerformed

    private void chkCambiarRepoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkCambiarRepoActionPerformed
       if(chkCambiarRepo.isSelected()==true){
        txtStockReposicionArt.setEditable(true);
       }else{
           if(chkCambiarRepo.isSelected()==false){
           txtStockReposicionArt.setEditable(false);
           }
       }
    }//GEN-LAST:event_chkCambiarRepoActionPerformed

    private void btnBuscarProveedorArtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarProveedorArtActionPerformed
        try{
            Proveedor unProveedor;
            unProveedor=unMercadito.getUnaSucursal().BuscarProveedor(this.txtCuitProvActualizar.getText());
            this.txtRazonSocialActualizar.setText(unProveedor.getRazonSocial());
            btnBuscarProveedorArt.setEnabled(false);
            txtCuitProvActualizar.setEditable(false);
            txtRazonSocialActualizar.setEditable(false);
            btnCambiarProveedor.setEnabled(true);
        }catch (Exception ex){
                 Logger.getLogger(InternalFrameCompra.class.getName()).log(Level.SEVERE, null, ex);
                 JOptionPane.showMessageDialog(this, "¡Codigo de Barras Inexistente!");
        }
    }//GEN-LAST:event_btnBuscarProveedorArtActionPerformed

    private void btnCambiarProveedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCambiarProveedorActionPerformed
        btnBuscarProveedorArt.setEnabled(true);
        txtCuitProvActualizar.setEditable(true);
        txtRazonSocialActualizar.setEditable(false);
        btnCambiarProveedor.setEnabled(false);
    }//GEN-LAST:event_btnCambiarProveedorActionPerformed

    private void chkCambiarMinActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkCambiarMinActionPerformed
        if(chkCambiarMin.isSelected()==true){
        txtStockMinimoArt.setEditable(true);
       }else{
           if(chkCambiarMin.isSelected()==false){
           txtStockMinimoArt.setEditable(false);
           }
       }
    }//GEN-LAST:event_chkCambiarMinActionPerformed

    private void txtStockMinimoArtKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtStockMinimoArtKeyTyped
        char c=evt.getKeyChar();
        if(Character.isLetter(c)) {
            getToolkit().beep();
            evt.consume();
            JOptionPane.showMessageDialog(this, "Este campo solo permite numeros.");
        }
    }//GEN-LAST:event_txtStockMinimoArtKeyTyped

    private void txtStockReposicionArtKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtStockReposicionArtKeyTyped
       char c=evt.getKeyChar();
        if(Character.isLetter(c)) {
            getToolkit().beep();
            evt.consume();
            JOptionPane.showMessageDialog(this, "Este campo solo permite numeros.");
        }
    }//GEN-LAST:event_txtStockReposicionArtKeyTyped

    private void btnAgregarArticuloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarArticuloActionPerformed
        try{    
            // Busco la descripcion del articulo seleccionado
            DescripcionArticulo unaDescripcionArticulo;
            unaDescripcionArticulo=this.unMercadito.getUnaSucursal().BuscarDescripcionArticulo(this.txtCodArticuloOrdenCompra.getText());          
            int cantidad = Integer.parseInt(this.txtCantArticuloOrdenCompra.getText());
            float precioUnitario=unaDescripcionArticulo.getPrecioCompra();
            float subtotal= cantidad * precioUnitario;
            total = total + subtotal;
            Vector <String> datos = new Vector();
            contador++;
            datos.add(String.valueOf(contador));
            datos.add(unaDescripcionArticulo.getCodigoBarra());
            datos.add(unaDescripcionArticulo.getNombreArticulo());
            datos.add(this.txtCantArticuloOrdenCompra.getText());
            datos.add(String.valueOf(unaDescripcionArticulo.getPrecioCompra()));
            datos.add(String.valueOf(subtotal));
            this.listaOrdenCompra.addRow(datos);
            this.tableOrdenCompra.setModel(listaOrdenCompra);
            txtCostoTotalOrdenCompra.setText(String.valueOf(total));
        }catch (Exception ex){
            Logger.getLogger(agregarArticulo.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(this, "¡Codigo de Barras Inexistente!");
        }      
        
    }//GEN-LAST:event_btnAgregarArticuloActionPerformed

    private void txtCantArticuloOrdenCompraKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCantArticuloOrdenCompraKeyTyped
        char c=evt.getKeyChar();
        if(Character.isLetter(c)) {
            getToolkit().beep();
            evt.consume();
            JOptionPane.showMessageDialog(this, "Este campo solo permite numeros.");
        }
    }//GEN-LAST:event_txtCantArticuloOrdenCompraKeyTyped

    private void btnEliminarFilaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarFilaActionPerformed
        int indice = tableOrdenCompra.getSelectedRow();
        float subtotalFila = Float.parseFloat((String) (listaOrdenCompra.getValueAt(indice, 5)));
        total = total - subtotalFila;
        txtCostoTotalOrdenCompra.setText(String.valueOf(total));
        contador--;
        listaOrdenCompra.removeRow(indice);
        tableOrdenCompra.setModel(listaOrdenCompra);
    }//GEN-LAST:event_btnEliminarFilaActionPerformed

    private void btnBuscarProveedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarProveedorActionPerformed
        try{
            Proveedor unProveedor;
            unProveedor=unMercadito.getUnaSucursal().BuscarProveedor(this.txtBuscarProveedorOrdenCompra.getText());
            this.txtNombreProveedor.setText(unProveedor.getRazonSocial());
        }catch (Exception ex){
                 Logger.getLogger(InternalFrameCompra.class.getName()).log(Level.SEVERE, null, ex);
                 JOptionPane.showMessageDialog(this, "¡CUIT proveedor Inexistente!");
        }
    }//GEN-LAST:event_btnBuscarProveedorActionPerformed

    private void btnBuscarEmpleadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarEmpleadoActionPerformed
        try{
            Empleado unEmpleado;
            unEmpleado = unMercadito.getUnaSucursal().BuscarEmpleado(this.txtBuscarEmpleado.getText());
            this.txtNombreEmpleado.setText(unEmpleado.getApellido() +" "+ unEmpleado.getNombre());
        }catch (Exception ex){
                 Logger.getLogger(InternalFrameCompra.class.getName()).log(Level.SEVERE, null, ex);
                 JOptionPane.showMessageDialog(this, "¡CUIT proveedor Inexistente!");
        }
    }//GEN-LAST:event_btnBuscarEmpleadoActionPerformed

    private void btnRegistrarOrdenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegistrarOrdenActionPerformed
//        int nroCompra = this.unMercadito.getUnaSucursal().UltimoNroCompra();
        try{
            int nroCompra = this.unMercadito.getUnaSucursal().UltimoNroCompra();
            Date fechaPedidoOrden = fechaPedido.getDate();
            Date fechaEntrega = null;         
            this.unMercadito.getUnaSucursal().AgregarUnaCompra(nroCompra, fechaPedidoOrden, fechaEntrega, this.txtTerminoEntrega.getText(), this.txtDireccionEntrega.getText(), this.txtNombreEmpleado.getText(), nroCompra);
            registrarDetallesOrdenCompra();
            JOptionPane.showMessageDialog(this, "¡Registro guardado!");
         
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "¡No se pudo registrar la Orden de Compra!");
            
        }
//         this.txtPrueba.setText(String.valueOf(nroCompra));
    }//GEN-LAST:event_btnRegistrarOrdenActionPerformed

    private void btnImprimirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImprimirActionPerformed
        try {
            createPdf();
            JOptionPane.showMessageDialog(this, "¡Se ha generado correctamente el PDF!");
        }catch(Exception ex){
            JOptionPane.showMessageDialog(this, "¡No se pudo generar el PDF!");
        }
    }//GEN-LAST:event_btnImprimirActionPerformed

    private void btnCancelarAgregarArticuloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarAgregarArticuloActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnCancelarAgregarArticuloActionPerformed

    private void btnBuscarProveedorFacturaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarProveedorFacturaActionPerformed
         try{
            Proveedor unProveedor;
            unProveedor=unMercadito.getUnaSucursal().BuscarProveedor(this.txtCuitFactura.getText());
            this.txtRazonSocialFactura.setText(unProveedor.getRazonSocial());
            this.txtCondIvaFactura.setText(unProveedor.getSituacionTributaria());
            
        }catch (Exception ex){
                 Logger.getLogger(InternalFrameCompra.class.getName()).log(Level.SEVERE, null, ex);
                 JOptionPane.showMessageDialog(this, "¡CUIT proveedor Inexistente!");
        
        }                             
    }//GEN-LAST:event_btnBuscarProveedorFacturaActionPerformed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
      txtNumFactura.setText("");
      txtCuitFactura.setText("");
      txtRazonSocialFactura.setText("");
      txtCondIvaFactura.setText("");
      txtFechaFactura.setToolTipText("");
      txtTotalFactura.setText("");
      txtTipoCompra.setText("");
        txtTipoCompra.setEnabled(true);
        txtNumFactura.setEditable(true);
      txtCuitFactura.setEditable(true);
      txtRazonSocialFactura.setEditable(true);
      txtCondIvaFactura.setEditable(true);
      txtFechaFactura.setToolTipText("");
      txtTotalFactura.setEditable(true);
      txtNumFactura.grabFocus();
      
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnGuardarFacturaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarFacturaActionPerformed
 try{
            Date fechaCompra = txtFechaFactura.getDate();       
            this.unMercadito.getUnaSucursal().AgregarUnRegistroFactura(Integer.parseInt(this.txtNumFactura.getText()), Integer.parseInt(this.txtCuitFactura.getText()), txtRazonSocialFactura.getText(), this.txtCondIvaFactura.getText(), fechaCompra, Float.parseFloat(this.txtTotalFactura.getText()), this.txtTipoCompra.getText());
            JOptionPane.showMessageDialog(this, "¡Registro guardado!");
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "¡No se pudo registrar la Factura de Compra!");
        
    }                                                         // TODO add your handling code here:
    }//GEN-LAST:event_btnGuardarFacturaActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
      txtNumFactura.setText("");
      txtCuitFactura.setText("");
      txtRazonSocialFactura.setText("");
      txtCondIvaFactura.setText("");
      txtFechaFactura.setToolTipText("");
      txtTotalFactura.setText("");
      txtTipoCompra.setText("");
      txtTipoCompra.setEditable(false);
      txtNumFactura.setEditable(false);
      txtCuitFactura.setEditable(false);
      txtRazonSocialFactura.setEditable(false);
      txtCondIvaFactura.setEditable(false);
      txtFechaFactura.setToolTipText("");
      txtTotalFactura.setEditable(false);
      
  
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void txtCambiarProveedorFacturaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCambiarProveedorFacturaActionPerformed
     
      txtCuitFactura.setText("");
      txtRazonSocialFactura.setText("");
      txtCondIvaFactura.setText("");

    }//GEN-LAST:event_txtCambiarProveedorFacturaActionPerformed

    private void txtCondIvaFacturaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCondIvaFacturaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCondIvaFacturaActionPerformed

    private void btnNuevaOrdenCompraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevaOrdenCompraActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnNuevaOrdenCompraActionPerformed

    private void btnCancelarOrdenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarOrdenActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnCancelarOrdenActionPerformed

    private void txtStockActualArtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtStockActualArtActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtStockActualArtActionPerformed

    private void txtRazonSocialFacturaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtRazonSocialFacturaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtRazonSocialFacturaActionPerformed
    public void limpiarCampos(){
        txtCantArt.setText("");
        txtStockActualDepActualizar.setText("");
        txtStockReposicionArt.setText("");
        txtCuitProvActualizar.setText("");
        txtRazonSocialActualizar.setText("");
    }
    
    
    public void registrarDetallesOrdenCompra() throws Exception{
        int idDetalle;
        String codBarra;
        String articulo;
        int cantidad;
        float costoUnitario;
        float subtotalFila;
        for (int i = 0; i < listaOrdenCompra.getRowCount(); i++) {
            idDetalle = this.unMercadito.getUnaSucursal().UltimoIdDetalleOrdenCompra();
            codBarra = String.valueOf(listaOrdenCompra.getValueAt(i, 1));
            articulo = String.valueOf(listaOrdenCompra.getValueAt(i, 2));
            cantidad = Integer.parseInt((String) listaOrdenCompra.getValueAt(i, 3));
            costoUnitario = Float.parseFloat((String) listaOrdenCompra.getValueAt(i, 4));
            subtotalFila = Float.parseFloat((String) listaOrdenCompra.getValueAt(i, 5));
            
            
            this.unMercadito.getUnaSucursal().AgregarUnRegOrdenCompra(idDetalle, codBarra, articulo, cantidad, costoUnitario, subtotalFila, this.txtNombreProveedor.getText(), this.fechaPedido.getDate());
        }
    }
    
    public String obtenerFecha(){
        try {
            String formato = fechaPedido.getDateFormatString();
            Date date = fechaPedido.getDate();
            SimpleDateFormat sdf = new SimpleDateFormat(formato);
            //txtFechaSel.setText(String.valueOf(sdf.format(date)));
            String fechaVenta = String.valueOf(sdf.format(date));
            return fechaVenta;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Seleccione una fecha ", "Error..!!", JOptionPane.ERROR_MESSAGE);
        }
        return null;
    }
    
    
    //PARA CREAR EL PDF///////////////////////////////////////////////////////////////////
    public static String archivo = System.getProperty("user.dir") + "/OrdenesDeCompra/ordenCompra.pdf";
    public void createPdf() throws FileNotFoundException {
        /*Declaramos documento como un objeto Document
         *Asignamos el tamaño de hoja y los margenes 
         */
        Document documento = new Document(PageSize.LETTER, 80, 80, 75, 75);

        //writer es declarado como el método utilizado para escribir en el archivo
        PdfWriter writer = null;
        try {
            writer = PdfWriter.getInstance(documento, new FileOutputStream(archivo));
        } catch (DocumentException ex) {
            Logger.getLogger(InternalFrameCaja.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(InternalFrameCaja.class.getName()).log(Level.SEVERE, null, ex);
        }

        //Agregamos un titulo al archivo
        documento.addTitle("El mercadito - Sucursal Posadas");

        //Agregamos el autor del archivo
        documento.addAuthor("Vargas Juan Antonio - Diaz Norberto Daniel");

        //Abrimos el documento para edición
        documento.open();

        //Declaramos un texto como Paragraph
        //Le podemos dar formado como alineación, tamaño y color a la fuente.
        Paragraph parrafo = new Paragraph();
        parrafo.setAlignment(Paragraph.ALIGN_CENTER);
        parrafo.setFont(FontFactory.getFont("Sans", 14, Font.BOLD, BaseColor.BLUE));
        parrafo.add("El mercadito - Sucursal Posadas - CUIT Nro: 30-61292945-5");
        try {
            //Agregamos el texto al documento
            documento.add(parrafo);
        } catch (DocumentException ex) {
            ex.getMessage();
        }
        //parrafo.clear();
        parrafo = new Paragraph();
        parrafo.setAlignment(Paragraph.ALIGN_CENTER);
        parrafo.setFont(FontFactory.getFont("Sans", 12, Font.BOLD, BaseColor.BLACK));
        parrafo.add("Orden de Compra");

        try {
            //Agregamos el texto al documento
            documento.add(parrafo);
        } catch (DocumentException ex) {
            ex.getMessage();
        }
        parrafo.clear();
        parrafo.setAlignment(Paragraph.ALIGN_LEFT);
        parrafo.setFont(FontFactory.getFont("Sans", 12, Font.NORMAL, BaseColor.BLACK));
        parrafo.add("Proveedor: " + txtNombreProveedor.getText() + " - Condición ante el IVA: Responsable Inscripto");

        try {
            //Agregamos el texto al documento
            documento.add(parrafo);
        } catch (DocumentException ex) {
            ex.getMessage();
        }
        ////////////////////
        parrafo.clear();
        parrafo.setAlignment(Paragraph.ALIGN_LEFT);
        parrafo.setFont(FontFactory.getFont("Sans", 12, Font.NORMAL, BaseColor.BLACK));
        String fecha = obtenerFecha();
        parrafo.add("Fecha de pedido: " + fecha + "  -  - - -    " + txtTerminoEntrega.getText());
        try {
            //Agregamos el texto al documento
            documento.add(parrafo);
        } catch (DocumentException ex) {
            ex.getMessage();
        }
        ////////////////////
        parrafo.clear();
        parrafo.setAlignment(Paragraph.ALIGN_LEFT);
        parrafo.setFont(FontFactory.getFont("Sans", 12, Font.NORMAL, BaseColor.BLACK));
        parrafo.add("Dirección de Entrega: " + txtDireccionEntrega.getText());
        try {
            //Agregamos el texto al documento
            documento.add(parrafo);
        } catch (DocumentException ex) {
            ex.getMessage();
        }
        //////////////////////////////////
        ////////////////////
        parrafo.clear();
        parrafo.setAlignment(Paragraph.ALIGN_LEFT);
        parrafo.setFont(FontFactory.getFont("Sans", 12, Font.NORMAL, BaseColor.BLACK));
        parrafo.add("Autorizado por: " + txtNombreEmpleado.getText() + "    --    CUIT: " + txtBuscarEmpleado.getText());
        try {
            //Agregamos el texto al documento
            documento.add(parrafo);
        } catch (DocumentException ex) {
            ex.getMessage();
        }
        //////////////////////////////////
        ////////////////////
        parrafo.clear();
        parrafo.setAlignment(Paragraph.ALIGN_CENTER);
        parrafo.setFont(FontFactory.getFont("Sans", 18, Font.NORMAL, BaseColor.BLACK));
        parrafo.add("__________________________________________________________________");
        try {
            //Agregamos el texto al documento
            documento.add(parrafo);
        } catch (DocumentException ex) {
            ex.getMessage();
        }
        //////////////////////////////////
        ////////////////////
        parrafo.clear();
        parrafo.setAlignment(Paragraph.ALIGN_CENTER);
        parrafo.setFont(FontFactory.getFont("Sans", 18, Font.NORMAL, BaseColor.BLACK));
        parrafo.add("   ");
        try {
            //Agregamos el texto al documento
            documento.add(parrafo);
        } catch (DocumentException ex) {
            ex.getMessage();
        }
        //////////////////////////////////
        PdfPTable table = new PdfPTable(6);

        PdfPCell cell = null;
       cell = new PdfPCell(new Phrase("Detalle Orden Compra"));
       

        cell.setColspan(6);
        cell.setBackgroundColor(BaseColor.GRAY);       

        table.addCell(cell);

        float miMedida[] = new float[6];
        miMedida[0] = 80;
        miMedida[1] = 80;
        miMedida[2] = 150;
        miMedida[3] = 80;
        miMedida[4] = 80;
        miMedida[5] = 80;
        try {
            table.setWidths(miMedida);
        } catch (DocumentException ex) {
            Logger.getLogger(InternalFrameCaja.class.getName()).log(Level.SEVERE, null, ex);
        }
//Añadir dos filas de celdas sin formato
        table.addCell("#");
        table.addCell("Codigo");
        table.addCell("Descripción");
        table.addCell("Cantidad");
        table.addCell("$ Unitario");
        table.addCell("Subtotal");
        
        
        
        
        for (int i = 0; i < listaOrdenCompra.getRowCount(); i++) {
            table.addCell(String.valueOf(listaOrdenCompra.getValueAt(i, 0)));
            table.addCell(String.valueOf(listaOrdenCompra.getValueAt(i, 1)));
            table.addCell(String.valueOf(listaOrdenCompra.getValueAt(i, 2)));
            table.addCell(String.valueOf(listaOrdenCompra.getValueAt(i, 3)));
            table.addCell(String.valueOf(listaOrdenCompra.getValueAt(i, 4)));
            table.addCell(String.valueOf(listaOrdenCompra.getValueAt(i, 5)));
        }
        table.addCell("TOTAL");
        table.addCell(" ");
        table.addCell(" ");
        table.addCell(" ");
        table.addCell(" ");
        table.addCell(String.valueOf(txtCostoTotalOrdenCompra.getText()));

        try {
            documento.add(table);
        } catch (DocumentException ex) {
            Logger.getLogger(InternalFrameCaja.class.getName()).log(Level.SEVERE, null, ex);
        }

        documento.close(); //Cerramos el documento
        writer.close(); //Cerramos writer
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnActualizarStock;
    private javax.swing.JButton btnAgregarArticulo;
    private javax.swing.JButton btnBuscarArt;
    private javax.swing.JButton btnBuscarEmpleado;
    private javax.swing.JButton btnBuscarProveedor;
    private javax.swing.JButton btnBuscarProveedorArt;
    private javax.swing.JButton btnBuscarProveedorFactura;
    private javax.swing.JButton btnCambiarProveedor;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnCancelarAgregarArticulo;
    private javax.swing.JButton btnCancelarOrden;
    private javax.swing.JButton btnEliminarFila;
    private javax.swing.JButton btnGuardarFactura;
    private javax.swing.JButton btnImprimir;
    private javax.swing.JButton btnNuevaOrdenCompra;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JButton btnRegistrarOrden;
    private javax.swing.JCheckBox chkCambiarMin;
    private javax.swing.JCheckBox chkCambiarRepo;
    private com.toedter.calendar.JDateChooser fechaPedido;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JPanel jPanelGeneraComprobante;
    private javax.swing.JPanel jPanelIngComprobante;
    private javax.swing.JScrollBar jScrollBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTabbedPane jTabbedPCompra;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JLabel lblFechaPedido;
    private javax.swing.JLabel lblProveedor;
    private javax.swing.JTable tableOrdenCompra;
    private javax.swing.JTextField txtBuscarArt;
    private javax.swing.JTextField txtBuscarEmpleado;
    private javax.swing.JTextField txtBuscarProveedorOrdenCompra;
    private javax.swing.JButton txtCambiarProveedorFactura;
    private javax.swing.JTextField txtCantArt;
    private javax.swing.JTextField txtCantArticuloOrdenCompra;
    private javax.swing.JTextField txtCantMinima;
    private javax.swing.JTextField txtCantUnMedidaArt;
    private javax.swing.JTextField txtCodArticuloOrdenCompra;
    private javax.swing.JTextField txtCodBarrasArt;
    private javax.swing.JTextField txtCondIvaFactura;
    private javax.swing.JTextField txtCostoTotalOrdenCompra;
    private javax.swing.JTextField txtCuitFactura;
    private javax.swing.JTextField txtCuitProvActualizar;
    private javax.swing.JTextField txtDescripcionArt;
    private javax.swing.JTextField txtDireccionEntrega;
    private com.toedter.calendar.JDateChooser txtFechaFactura;
    private javax.swing.JTextField txtNombreArt;
    private javax.swing.JTextField txtNombreEmpleado;
    private javax.swing.JTextField txtNombreProveedor;
    private javax.swing.JTextField txtNumFactura;
    private javax.swing.JTextField txtPrecioCompraArt;
    private javax.swing.JTextField txtPrecioVtaArt;
    private javax.swing.JTextField txtPrecioVtaMayArt;
    private javax.swing.JTextField txtRazonSocialActualizar;
    private javax.swing.JTextField txtRazonSocialFactura;
    private javax.swing.JTextField txtStockActualArt;
    private javax.swing.JTextField txtStockActualDepActualizar;
    private javax.swing.JTextField txtStockMinimoArt;
    private javax.swing.JTextField txtStockReposicionArt;
    private javax.swing.JTextField txtTerminoEntrega;
    private javax.swing.JTextField txtTipoCompra;
    private javax.swing.JTextField txtTipoEnvaseArt;
    private javax.swing.JTextField txtTotalFactura;
    private javax.swing.JTextField txtUnidadMedidaArt;
    private javax.swing.JTextField txtUnidadesPorBulto;
    // End of variables declaration//GEN-END:variables
}
