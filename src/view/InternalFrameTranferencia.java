/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package view;

import ElMercadito.ElMercadito;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.HeadlessException;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import model.Deposito;
import model.DescripcionArticulo;
import model.Gondola;

/**
 *
 * @author Antonio
 */
public class InternalFrameTranferencia extends javax.swing.JInternalFrame {
    private ElMercadito unMercadito;
    private DefaultTableModel modeloArticulosEnGondola = new DefaultTableModel();
    private DefaultTableModel modeloTranferencia = new DefaultTableModel();
    String numeroRemito;
    public InternalFrameTranferencia(ElMercadito unMercadito) {
        this.unMercadito = unMercadito;
        initComponents();
        //Tranferencia Deposito -> Gondola
        modeloTranferencia.addColumn("Codigo Barra");
        modeloTranferencia.addColumn("Descripcion Artículo");
        modeloTranferencia.addColumn("Cantidad");
        this.JTableListaTranferencia.setModel(modeloTranferencia);
    
        modeloArticulosEnGondola.addColumn("Cod. Barra");
        modeloArticulosEnGondola.addColumn("Articulo");
        modeloArticulosEnGondola.addColumn("Sector");
        modeloArticulosEnGondola.addColumn("Tipo Góndola");
        modeloArticulosEnGondola.addColumn("Stock Mínimo");
        modeloArticulosEnGondola.addColumn("Stock Actual");
          
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        nroRemito = new javax.swing.JTextField();
        jPanel5 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtTipoTrans = new javax.swing.JTextField();
        txtOrigenTrans = new javax.swing.JTextField();
        txtDestinoTrans = new javax.swing.JTextField();
        txtFechaTrans = new com.toedter.calendar.JDateChooser();
        txtHoraTrans = new javax.swing.JTextField();
        jPanel6 = new javax.swing.JPanel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        txtCodArticulo = new javax.swing.JTextField();
        txtCantArticulo = new javax.swing.JTextField();
        btnCargarArticulo = new javax.swing.JButton();
        btnCancelarArticulo = new javax.swing.JButton();
        JScrollListaTranferencia = new javax.swing.JScrollPane();
        JTableListaTranferencia = new javax.swing.JTable();
        btnNuevaTrans = new javax.swing.JButton();
        btnImprimirTrans = new javax.swing.JButton();
        btnRegistrarTransferencia = new javax.swing.JButton();
        btnCancelarTrans = new javax.swing.JButton();
        btnQuitarFila = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        lblCodBarraArticulo = new javax.swing.JLabel();
        txtBuscarArt = new javax.swing.JTextField();
        btnBuscarArt = new javax.swing.JButton();
        txtDescripcionArt = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        lblNombreArticulo = new javax.swing.JLabel();
        txtNombreArt = new javax.swing.JTextField();
        jPanel20 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        lblStockActualArticulo = new javax.swing.JLabel();
        txtStockActualArtDep = new javax.swing.JTextField();
        jPanel8 = new javax.swing.JPanel();
        lblStockActualArticulo1 = new javax.swing.JLabel();
        txtStockActualArtGon = new javax.swing.JTextField();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Tanferencias");
        setMaximumSize(new java.awt.Dimension(600, 680));
        setMinimumSize(new java.awt.Dimension(600, 680));
        setPreferredSize(new java.awt.Dimension(600, 680));
        setRequestFocusEnabled(false);

        jLabel18.setFont(new java.awt.Font("Traditional Arabic", 1, 24)); // NOI18N
        jLabel18.setText("El Mercadito S.A.");

        jLabel19.setText("Sucursal Posadas - Misiones");

        jLabel26.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel26.setForeground(new java.awt.Color(153, 0, 0));
        jLabel26.setText("Remito Interno de Tranferencia");

        jLabel27.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel27.setForeground(new java.awt.Color(153, 0, 0));
        jLabel27.setText("Depósito a Góndola");

        jLabel6.setText("N°");

        nroRemito.setEditable(false);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(166, 166, 166))
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(198, 198, 198)
                        .addComponent(jLabel19))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(178, 178, 178)
                        .addComponent(jLabel26)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel27)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(nroRemito)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel18)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel19)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel26)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel27)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 9, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(nroRemito, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jPanel5.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));

        jLabel1.setText("Tipo Tranferencia:");

        jLabel2.setText("Origen:");

        jLabel3.setText("Destino:");

        jLabel4.setText("Fecha:");

        jLabel5.setText("Hora:");

        txtTipoTrans.setEditable(false);
        txtTipoTrans.setText("Interna");

        txtOrigenTrans.setEditable(false);
        txtOrigenTrans.setText("Depósito");

        txtDestinoTrans.setEditable(false);
        txtDestinoTrans.setText("Góndola");

        txtHoraTrans.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtHoraTransActionPerformed(evt);
            }
        });

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true), "Artículos", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 11))); // NOI18N

        jLabel22.setText("Cod. Artículo:");

        jLabel23.setText("Cantidad:");

        txtCantArticulo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCantArticuloKeyTyped(evt);
            }
        });

        btnCargarArticulo.setText("Agregar");
        btnCargarArticulo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCargarArticuloActionPerformed(evt);
            }
        });

        btnCancelarArticulo.setText("Cancelar");
        btnCancelarArticulo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarArticuloActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel22)
                            .addComponent(jLabel23))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtCodArticulo)
                            .addComponent(txtCantArticulo)))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGap(33, 33, 33)
                        .addComponent(btnCargarArticulo)
                        .addGap(18, 18, 18)
                        .addComponent(btnCancelarArticulo)
                        .addGap(0, 42, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel22)
                    .addComponent(txtCodArticulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel23)
                    .addComponent(txtCantArticulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 26, Short.MAX_VALUE)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCargarArticulo)
                    .addComponent(btnCancelarArticulo)))
        );

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel5Layout.createSequentialGroup()
                            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel1)
                                .addComponent(jLabel2))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(txtTipoTrans)
                                .addComponent(txtOrigenTrans, javax.swing.GroupLayout.DEFAULT_SIZE, 54, Short.MAX_VALUE)))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel5Layout.createSequentialGroup()
                            .addComponent(jLabel3)
                            .addGap(52, 52, 52)
                            .addComponent(txtDestinoTrans)))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtHoraTrans, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtFechaTrans, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(70, 70, 70)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(122, 122, 122))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(txtTipoTrans, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(11, 11, 11)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(txtOrigenTrans, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtDestinoTrans, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(txtFechaTrans, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(txtHoraTrans, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        JTableListaTranferencia.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        JScrollListaTranferencia.setViewportView(JTableListaTranferencia);

        btnNuevaTrans.setText("Nueva Transferencia");
        btnNuevaTrans.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevaTransActionPerformed(evt);
            }
        });

        btnImprimirTrans.setText("Imprimir");
        btnImprimirTrans.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImprimirTransActionPerformed(evt);
            }
        });

        btnRegistrarTransferencia.setText("Registrar");
        btnRegistrarTransferencia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegistrarTransferenciaActionPerformed(evt);
            }
        });

        btnCancelarTrans.setText("Cancelar");
        btnCancelarTrans.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarTransActionPerformed(evt);
            }
        });

        btnQuitarFila.setText("Eliminar Fila");
        btnQuitarFila.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnQuitarFilaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(JScrollListaTranferencia)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnNuevaTrans)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnImprimirTrans)
                .addGap(18, 18, 18)
                .addComponent(btnQuitarFila)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnRegistrarTransferencia)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnCancelarTrans)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, 580, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(JScrollListaTranferencia, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(38, 38, 38)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnNuevaTrans)
                    .addComponent(btnImprimirTrans)
                    .addComponent(btnRegistrarTransferencia)
                    .addComponent(btnCancelarTrans)
                    .addComponent(btnQuitarFila))
                .addGap(0, 41, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Depósito -> Góndola", jPanel3);

        lblCodBarraArticulo.setText("Código de barra:");

        btnBuscarArt.setText("Buscar");
        btnBuscarArt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarArtActionPerformed(evt);
            }
        });

        jLabel8.setText("Descripción:");

        lblNombreArticulo.setText("Nombre:");

        jPanel20.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true), "Artículo"));
        jPanel20.addVetoableChangeListener(new java.beans.VetoableChangeListener() {
            public void vetoableChange(java.beans.PropertyChangeEvent evt)throws java.beans.PropertyVetoException {
                jPanel20jPanel2VetoableChange(evt);
            }
        });

        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true), "Stock en Depósito", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 11))); // NOI18N

        lblStockActualArticulo.setText("Stock Actual:");

        txtStockActualArtDep.setEditable(false);
        txtStockActualArtDep.setText("0");

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblStockActualArticulo)
                .addGap(10, 10, 10)
                .addComponent(txtStockActualArtDep, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(50, Short.MAX_VALUE))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblStockActualArticulo)
                    .addComponent(txtStockActualArtDep, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel8.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true), "Stock en Góndola", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 11))); // NOI18N

        lblStockActualArticulo1.setText("Stock Actual:");

        txtStockActualArtGon.setEditable(false);
        txtStockActualArtGon.setText("0");

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblStockActualArticulo1)
                .addGap(10, 10, 10)
                .addComponent(txtStockActualArtGon, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(50, Short.MAX_VALUE))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblStockActualArticulo1)
                    .addComponent(txtStockActualArtGon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel20Layout = new javax.swing.GroupLayout(jPanel20);
        jPanel20.setLayout(jPanel20Layout);
        jPanel20Layout.setHorizontalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel20Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(74, 74, 74)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(64, Short.MAX_VALUE))
        );
        jPanel20Layout.setVerticalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel20Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblNombreArticulo)
                        .addGap(47, 47, 47)
                        .addComponent(txtNombreArt, javax.swing.GroupLayout.PREFERRED_SIZE, 196, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(33, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(lblCodBarraArticulo)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(txtBuscarArt, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(40, 40, 40)
                            .addComponent(btnBuscarArt))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGap(91, 91, 91)
                            .addComponent(txtDescripcionArt)))
                    .addGap(286, 286, 286)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(43, 43, 43)
                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNombreArt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblNombreArticulo))
                .addGap(18, 18, 18)
                .addComponent(jPanel20, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(402, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnBuscarArt)
                        .addComponent(txtBuscarArt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblCodBarraArticulo, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(txtDescripcionArt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(571, Short.MAX_VALUE)))
        );

        jTabbedPane1.addTab("Artículos en Stock", jPanel1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 572, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 12, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCargarArticuloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCargarArticuloActionPerformed
       try{
           DescripcionArticulo unaDescripcionArticulo;
           unaDescripcionArticulo=this.unMercadito.getUnaSucursal().BuscarDescripcionArticulo(this.txtCodArticulo.getText());
           Deposito unDeposito;
           unDeposito=this.unMercadito.getUnaSucursal().BuscarDeposito(this.txtCodArticulo.getText());
           //Verifica la cantidad ingresada sea menor o igual al Stock Actual en Depósito
           if(unDeposito.getStockActualDeposito() >= Integer.parseInt(txtCantArticulo.getText())){
               Vector <String> datos = new Vector();
               datos.add(unaDescripcionArticulo.getCodigoBarra());
               datos.add(unaDescripcionArticulo.getNombreArticulo());
               datos.add(String.valueOf(txtCantArticulo.getText()));
               this.modeloTranferencia.addRow(datos);
               this.JTableListaTranferencia.setModel(modeloTranferencia);
               String codBarra= txtCodArticulo.getText();
               int cantidad = Integer.parseInt(txtCantArticulo.getText());
               descontarStockActualDeposito(codBarra,cantidad); //Descuenta el stock actual en deposito, y aumenta el stock actual en gondola
               aumentarStockActualGondola(codBarra,cantidad);
           }else{
               JOptionPane.showMessageDialog(this, "El stock actual en depósito es:" + unDeposito.getStockActualDeposito());
           }           
       }catch (HeadlessException | NumberFormatException ex){
            Logger.getLogger(agregarArticulo.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(this, "¡Codigo de Barras Inexistente!");
        }
    }//GEN-LAST:event_btnCargarArticuloActionPerformed

    private void btnQuitarFilaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnQuitarFilaActionPerformed
        int indice = JTableListaTranferencia.getSelectedRow();
        String codBarra = String.valueOf(modeloTranferencia.getValueAt(indice, 0));
        int cantidad =  Integer.parseInt((String) modeloTranferencia.getValueAt(indice, 2));
        aumentarStockActualDeposito(codBarra, cantidad);
        descontarStockActuaGondola(codBarra, cantidad);
        modeloTranferencia.removeRow(indice);
        JTableListaTranferencia.setModel(modeloTranferencia);
    }//GEN-LAST:event_btnQuitarFilaActionPerformed

    private void txtCantArticuloKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCantArticuloKeyTyped
        char c=evt.getKeyChar();
        if(Character.isLetter(c)) {
            getToolkit().beep();
            evt.consume();
            JOptionPane.showMessageDialog(this, "Este campo solo permite numeros.");
        }
    }//GEN-LAST:event_txtCantArticuloKeyTyped

    private void btnRegistrarTransferenciaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegistrarTransferenciaActionPerformed
        try{
            int idTransferencia = this.unMercadito.getUnaSucursal().UltimoIdTransferencia();
            String fecha = obtenerFecha();
            this.unMercadito.getUnaSucursal().AgregarUnaTranferencia(idTransferencia, this.txtTipoTrans.getText(), this.txtOrigenTrans.getText(), this.txtDestinoTrans.getText(), fecha, this.txtHoraTrans.getText(), idTransferencia);
            btnQuitarFila.setEnabled(false);
            btnCancelarTrans.setEnabled(false);
            btnRegistrarTransferencia.setEnabled(false);
            btnRegistrarTransferencia.setEnabled(true);
            btnNuevaTrans.setEnabled(true);
            txtCodArticulo.setEditable(false);
            btnCargarArticulo.setEnabled(false);
            btnCancelarArticulo.setEnabled(false);
            btnImprimirTrans.setEnabled(true);
            nroRemito.setText(String.valueOf(idTransferencia));
            numeroRemito = String.valueOf(nroRemito.getText());
        }catch (Exception ex) {
            Logger.getLogger(InternalFrameTranferencia.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(this, "Error con la base de datos");
        } 
    }//GEN-LAST:event_btnRegistrarTransferenciaActionPerformed

    private void btnNuevaTransActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevaTransActionPerformed
        btnQuitarFila.setEnabled(true);
        btnCancelarTrans.setEnabled(true);
        btnRegistrarTransferencia.setEnabled(true);
        btnNuevaTrans.setEnabled(false);
        txtCodArticulo.setEditable(true);
        btnCargarArticulo.setEnabled(true);
        btnCancelarArticulo.setEnabled(true);
        btnImprimirTrans.setEnabled(false);
        nroRemito.setText("");
        deleteAllRows();
    }//GEN-LAST:event_btnNuevaTransActionPerformed
    
    public void deleteAllRows(){
        for( int i = modeloTranferencia.getRowCount() - 1; i >= 0; i-- ){
        modeloTranferencia.removeRow(i);
        }
    }
    
    
    private void btnCancelarTransActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarTransActionPerformed
        btnQuitarFila.setEnabled(false);
        btnCancelarTrans.setEnabled(false);
        btnRegistrarTransferencia.setEnabled(false);
        btnNuevaTrans.setEnabled(true);
        txtCodArticulo.setEditable(false);
        btnCargarArticulo.setEnabled(false);
        btnCancelarArticulo.setEnabled(false);
        btnImprimirTrans.setEnabled(false);
        nroRemito.setText("");
    }//GEN-LAST:event_btnCancelarTransActionPerformed

    private void btnImprimirTransActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImprimirTransActionPerformed
        try {
            createPdf();
        }catch(FileNotFoundException ex){}
    }//GEN-LAST:event_btnImprimirTransActionPerformed

    private void btnCancelarArticuloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarArticuloActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnCancelarArticuloActionPerformed

    private void txtHoraTransActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtHoraTransActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtHoraTransActionPerformed

    private void btnBuscarArtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarArtActionPerformed
        try{
            DescripcionArticulo unaDescripcionArticulo;
            unaDescripcionArticulo=unMercadito.getUnaSucursal().BuscarDescripcionArticulo(this.txtBuscarArt.getText());
            this.txtNombreArt.setText(unaDescripcionArticulo.getNombreArticulo());
            this.txtDescripcionArt.setText(unaDescripcionArticulo.getDescripcion());

            Deposito unDeposito;
            unDeposito=unMercadito.getUnaSucursal().BuscarDeposito(this.txtBuscarArt.getText());
            this.txtStockActualArtDep.setText(String.valueOf(unDeposito.getStockActualDeposito()));

            Gondola unaGondola;
            unaGondola=unMercadito.getUnaSucursal().BuscarGondola(this.txtBuscarArt.getText());
            this.txtStockActualArtGon.setText(String.valueOf(unaGondola.getStockActualGondola()));

        }catch (Exception ex){
            Logger.getLogger(InternalFrameCompra.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(this, "¡Codigo de Barras Inexistente!");
        }
    }//GEN-LAST:event_btnBuscarArtActionPerformed

    private void jPanel20jPanel2VetoableChange(java.beans.PropertyChangeEvent evt)throws java.beans.PropertyVetoException {//GEN-FIRST:event_jPanel20jPanel2VetoableChange
        // TODO add your handling code here:
    }//GEN-LAST:event_jPanel20jPanel2VetoableChange
    
    public void descontarStockActualDeposito(String codBarra, int cantidad){
        try{
            Deposito unDeposito = this.unMercadito.getUnaSucursal().BuscarDeposito(codBarra);
            cantidad = unDeposito.getStockActualDeposito() - cantidad;
            this.unMercadito.getUnaSucursal().ModificarDeposito(unDeposito, unDeposito.getNroDeposito(), unDeposito.getCodigoBarra(), unDeposito.getStockReposicion(), cantidad, unDeposito.getProveedor());
        } catch (Exception ex) {
            Logger.getLogger(InternalFrameTranferencia.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(this, "Error con la base de datos");
        }
    }
    public void aumentarStockActualDeposito(String codBarra, int cantidad){
        try{
            Deposito unDeposito = this.unMercadito.getUnaSucursal().BuscarDeposito(codBarra);
            cantidad = unDeposito.getStockActualDeposito() + cantidad;           
            this.unMercadito.getUnaSucursal().ModificarDeposito(unDeposito, unDeposito.getNroDeposito(), unDeposito.getCodigoBarra(), unDeposito.getStockReposicion(), cantidad, unDeposito.getProveedor());
        } catch (Exception ex) {
            Logger.getLogger(InternalFrameTranferencia.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(this, "Error con la base de datos");
        }
    }
    public void aumentarStockActualGondola(String codBarra, int cantidad){
        try{
            Gondola unaGondola= this.unMercadito.getUnaSucursal().BuscarGondola(codBarra);
            cantidad = unaGondola.getStockActualGondola() + cantidad;
            this.unMercadito.getUnaSucursal().ModificarGondola(unaGondola, unaGondola.getNroGondola(), unaGondola.getTipoGondola(), unaGondola.getCodigoBarra(), unaGondola.getStockMinimo(), cantidad, unaGondola.getSectorGondola());
            
        }catch (Exception ex) {
            Logger.getLogger(InternalFrameTranferencia.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(this, "Error con la base de datos");
        }
    }
    public void descontarStockActuaGondola(String codBarra, int cantidad){
        try{
            Gondola unaGondola= this.unMercadito.getUnaSucursal().BuscarGondola(codBarra);
            cantidad = unaGondola.getStockActualGondola() - cantidad;
            this.unMercadito.getUnaSucursal().ModificarGondola(unaGondola, unaGondola.getNroGondola(), unaGondola.getTipoGondola(), unaGondola.getCodigoBarra(), unaGondola.getStockMinimo(), cantidad, unaGondola.getSectorGondola());
            
        }catch (Exception ex) {
            Logger.getLogger(InternalFrameTranferencia.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(this, "Error con la base de datos");
        }
    }
       
        
    
public String obtenerFecha(){
    try {
        String formato = txtFechaTrans.getDateFormatString();
        Date date = txtFechaTrans.getDate();
        SimpleDateFormat sdf = new SimpleDateFormat(formato);
        //txtFechaSel.setText(String.valueOf(sdf.format(date)));
        String fechaTransferencia = String.valueOf(sdf.format(date));
        return fechaTransferencia;
        } catch (Exception e) {
        JOptionPane.showMessageDialog(null, "Al menos elija una FECHA VALIDA ", "Error..!!", JOptionPane.ERROR_MESSAGE);
        return null;
    }
}




///////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////CREAR PDF//////////////////////////////////////////////////////////////////////////////
    public String archivo = System.getProperty("user.dir") + "/remitoInternoTranferencia/" + numeroRemito + ".pdf";
    public void createPdf() throws FileNotFoundException {
        /*Declaramos documento como un objeto Document
         *Asignamos el tamaño de hoja y los margenes 
         */
        Document documento = new Document(PageSize.LETTER, 80, 80, 75, 75);

        //writer es declarado como el método utilizado para escribir en el archivo
        PdfWriter writer = null;
        try {
            writer = PdfWriter.getInstance(documento, new FileOutputStream(archivo));
        } catch (DocumentException ex) {
            Logger.getLogger(InternalFrameCaja.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(InternalFrameCaja.class.getName()).log(Level.SEVERE, null, ex);
        }

        //Agregamos un titulo al archivo
        documento.addTitle("El mercadito - Sucursal Posadas");

        //Agregamos el autor del archivo
        documento.addAuthor("Vargas Juan Antonio - Diaz Norberto Daniel");

        //Abrimos el documento para edición
        documento.open();

        //Declaramos un texto como Paragraph
        //Le podemos dar formado como alineación, tamaño y color a la fuente.
        Paragraph parrafo = new Paragraph();
        parrafo.setAlignment(Paragraph.ALIGN_CENTER);
        parrafo.setFont(FontFactory.getFont("Sans", 14, Font.BOLD, BaseColor.BLUE));
        parrafo.add("El mercadito - Sucursal Posadas - CUIT Nro: 30-61292945-5");
        try {
            //Agregamos el texto al documento
            documento.add(parrafo);
        } catch (DocumentException ex) {
            ex.getMessage();
        }
        //parrafo.clear();
        parrafo = new Paragraph();
        parrafo.setAlignment(Paragraph.ALIGN_CENTER);
        parrafo.setFont(FontFactory.getFont("Sans", 12, Font.BOLD, BaseColor.BLACK));
        parrafo.add("Remito Interno de Transferencia    -------  " + "Nro: Remito: " + nroRemito.getText());

        try {
            //Agregamos el texto al documento
            documento.add(parrafo);
        } catch (DocumentException ex) {
            ex.getMessage();
        }
        parrafo.clear();
        parrafo.setAlignment(Paragraph.ALIGN_LEFT);
        parrafo.setFont(FontFactory.getFont("Sans", 12, Font.NORMAL, BaseColor.BLACK));
        parrafo.add("Tipo Transferencia: " + txtTipoTrans.getText() + " - Origen: " + txtOrigenTrans.getText() + " - Destino: " + txtDestinoTrans.getText());

        try {
            //Agregamos el texto al documento
            documento.add(parrafo);
        } catch (DocumentException ex) {
            ex.getMessage();
        }
        ////////////////////
        parrafo.clear();
        parrafo.setAlignment(Paragraph.ALIGN_LEFT);
        parrafo.setFont(FontFactory.getFont("Sans", 12, Font.NORMAL, BaseColor.BLACK));
        String fecha = obtenerFecha();
        parrafo.add("Fecha: " + fecha + "   -    Hora: " + txtHoraTrans.getText());
        try {
            //Agregamos el texto al documento
            documento.add(parrafo);
        } catch (DocumentException ex) {
            ex.getMessage();
        }
        //////////////////////////////////
        ////////////////////
        parrafo.clear();
        parrafo.setAlignment(Paragraph.ALIGN_CENTER);
        parrafo.setFont(FontFactory.getFont("Sans", 18, Font.NORMAL, BaseColor.BLACK));
        parrafo.add("__________________________________________________________________");
        try {
            //Agregamos el texto al documento
            documento.add(parrafo);
        } catch (DocumentException ex) {
            ex.getMessage();
        }
        //////////////////////////////////
        ////////////////////
        parrafo.clear();
        parrafo.setAlignment(Paragraph.ALIGN_CENTER);
        parrafo.setFont(FontFactory.getFont("Sans", 18, Font.NORMAL, BaseColor.BLACK));
        parrafo.add("   ");
        try {
            //Agregamos el texto al documento
            documento.add(parrafo);
        } catch (DocumentException ex) {
            ex.getMessage();
        }
        //////////////////////////////////
        PdfPTable table = new PdfPTable(3);

        PdfPCell cell = null;
        cell = new PdfPCell(new Phrase("Detalle Remito"));
        cell.setVerticalAlignment(Element.ALIGN_CENTER);
        cell.setColspan(3);
        cell.setBackgroundColor(BaseColor.GRAY);       
        table.addCell(cell);
        float miMedida[] = new float[3];
        miMedida[0] = 80;
        miMedida[1] = 150;
        miMedida[2] = 80;
        try {
            table.setWidths(miMedida);
        } catch (DocumentException ex) {
            Logger.getLogger(InternalFrameTranferencia.class.getName()).log(Level.SEVERE, null, ex);
        }
//Añadir dos filas de celdas sin formato
        table.addCell("Cod. Barra");
        table.addCell("Producto");
        table.addCell("Cantidad");

        for (int i = 0; i < modeloTranferencia.getRowCount(); i++) {
            table.addCell(String.valueOf(modeloTranferencia.getValueAt(i, 0)));
            table.addCell(String.valueOf(modeloTranferencia.getValueAt(i, 1)));
            table.addCell(String.valueOf(modeloTranferencia.getValueAt(i, 2)));
        }

        try {
            documento.add(table);
        } catch (DocumentException ex) {
            Logger.getLogger(InternalFrameTranferencia.class.getName()).log(Level.SEVERE, null, ex);
        }

        documento.close(); //Cerramos el documento
        writer.close(); //Cerramos writer
    }




    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane JScrollListaTranferencia;
    private javax.swing.JTable JTableListaTranferencia;
    private javax.swing.JButton btnBuscarArt;
    private javax.swing.JButton btnCancelarArticulo;
    private javax.swing.JButton btnCancelarTrans;
    private javax.swing.JButton btnCargarArticulo;
    private javax.swing.JButton btnImprimirTrans;
    private javax.swing.JButton btnNuevaTrans;
    private javax.swing.JButton btnQuitarFila;
    private javax.swing.JButton btnRegistrarTransferencia;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel lblCodBarraArticulo;
    private javax.swing.JLabel lblNombreArticulo;
    private javax.swing.JLabel lblStockActualArticulo;
    private javax.swing.JLabel lblStockActualArticulo1;
    private javax.swing.JTextField nroRemito;
    private javax.swing.JTextField txtBuscarArt;
    private javax.swing.JTextField txtCantArticulo;
    private javax.swing.JTextField txtCodArticulo;
    private javax.swing.JTextField txtDescripcionArt;
    private javax.swing.JTextField txtDestinoTrans;
    private com.toedter.calendar.JDateChooser txtFechaTrans;
    private javax.swing.JTextField txtHoraTrans;
    private javax.swing.JTextField txtNombreArt;
    private javax.swing.JTextField txtOrigenTrans;
    private javax.swing.JTextField txtStockActualArtDep;
    private javax.swing.JTextField txtStockActualArtGon;
    private javax.swing.JTextField txtTipoTrans;
    // End of variables declaration//GEN-END:variables
}
