/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
/**
 *
 * @author daniel
 */
@Entity
@SuppressWarnings("serial")
public class RegistroFacturaCompra implements Serializable {
    @Id
    private int idNroFacturaCompra;
    @Basic
    private int nroProveedor;
    @Basic 
    private String razonSocial;
    @Basic
    private String condicionIva;
    @Basic
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date fechaCompra;
    @Basic
    private float totalCompra;
    @Basic
    private String TipoCompra;
 public RegistroFacturaCompra() {}

public RegistroFacturaCompra(int idNroFacturaCompra, int nroProveedor, String razonSocial, String condicionIva, Date fechaCompra, float totalCompra, String TipoCompra) {
        this.idNroFacturaCompra = idNroFacturaCompra;
        this.nroProveedor = nroProveedor;
        this.razonSocial = razonSocial;
        this.condicionIva = condicionIva;
        this.fechaCompra = fechaCompra;
        this.totalCompra = totalCompra;
        this.TipoCompra = TipoCompra;
    }

    public int getIdNroFacturaCompra() {
        return idNroFacturaCompra;
    }

    public int getNroProveedor() {
        return nroProveedor;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public String getCondicionIva() {
        return condicionIva;
    }

    public Date getFechaCompra() {
        return fechaCompra;
    }

    public float getTotalCompra() {
        return totalCompra;
    }

    public String getTipoVenta() {
        return TipoCompra;
    }

    public void setIdNroFacturaCompra(int idNroFacturaCompra) {
        this.idNroFacturaCompra = idNroFacturaCompra;
    }

    public void setNroProveedor(int nroProveedor) {
        this.nroProveedor = nroProveedor;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public void setCondicionIva(String condicionIva) {
        this.condicionIva = condicionIva;
    }

    public void setFechaCompra(Date fechaCompra) {
        this.fechaCompra = fechaCompra;
    }

    public void setTotalCompra(float totalCompra) {
        this.totalCompra = totalCompra;
    }

    public void setTipoCompra(String TipoCompra) {
        this.TipoCompra = TipoCompra;
    }
    
}
