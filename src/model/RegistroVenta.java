/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;

/**
 *
 * @author Antonio
 */
@Entity
public class RegistroVenta implements Serializable{
    @Id 
    private int idVenta;
    @Basic
    private String codigoBarra;
    @Basic
    private float precioVta;
    @Basic
    private int cantidad;
    @Basic    
    private float costoCompra;
    @Basic
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date fechaVenta;
    @Basic 
    private int nroComprobante;

    public RegistroVenta() {
    }

    public RegistroVenta(int idVenta, String codigoBarra, float precioVta, int cantidad, float costoCompra, Date fechaVenta, int nroComprobante) {
        this.idVenta = idVenta;
        this.codigoBarra = codigoBarra;
        this.precioVta = precioVta;
        this.cantidad = cantidad;
        this.costoCompra = costoCompra;
        this.fechaVenta = fechaVenta;
        this.nroComprobante = nroComprobante;
    }

    public int getIdVenta() {
        return idVenta;
    }

    public void setIdVenta(int idVenta) {
        this.idVenta = idVenta;
    }

    public String getCodigoBarra() {
        return codigoBarra;
    }

    public void setCodigoBarra(String codigoBarra) {
        this.codigoBarra = codigoBarra;
    }

    public float getPrecioVta() {
        return precioVta;
    }

    public void setPrecioVta(float precioVta) {
        this.precioVta = precioVta;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public float getCostoCompra() {
        return costoCompra;
    }

    public void setCostoCompra(float costoCompra) {
        this.costoCompra = costoCompra;
    }

    public Date getFechaVenta() {
        return fechaVenta;
    }

    public void setFechaVenta(Date fechaVenta) {
        this.fechaVenta = fechaVenta;
    }

    public int getNroComprobante() {
        return nroComprobante;
    }

    public void setNroComprobante(int nroComprobante) {
        this.nroComprobante = nroComprobante;
    }
    
}
