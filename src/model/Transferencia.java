/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.io.Serializable;
import java.util.ArrayList;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 *
 * @author Antonio
 */
@Entity
public class Transferencia implements Serializable {
    @Id
    private int idTranferencia;
    @Basic
    private String tipotransferencia;
    @Basic
    private String origen;
    @Basic
    private String destino;
    @Basic
    private String fecha;
    @Basic
    private String hora;
    @Basic
    private int nroRemitoTranferencia;
    
    
    public Transferencia(){}

    public Transferencia(int idTranferencia, String tipotransferencia, String origen, String destino, String fecha, String hora, int nroRemitoTranferencia) {
        this.idTranferencia = idTranferencia;
        this.tipotransferencia = tipotransferencia;
        this.origen = origen;
        this.destino = destino;
        this.fecha = fecha;
        this.hora = hora;
        this.nroRemitoTranferencia = nroRemitoTranferencia;
    }

    public int getIdTranferencia() {
        return idTranferencia;
    }

    public void setIdTranferencia(int idTranferencia) {
        this.idTranferencia = idTranferencia;
    }

    public String getTipotransferencia() {
        return tipotransferencia;
    }

    public void setTipotransferencia(String tipotransferencia) {
        this.tipotransferencia = tipotransferencia;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public int getNroRemitoTranferencia() {
        return nroRemitoTranferencia;
    }

    public void setNroRemitoTranferencia(int nroRemitoTranferencia) {
        this.nroRemitoTranferencia = nroRemitoTranferencia;
    }   
    
}

    

    
    
            
            
    

