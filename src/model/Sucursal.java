/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

//import javax.swing.text.html.parser.Entity;

import Persistencia.ControladoraPersistencia;
import Persistencia.exceptions.NonexistentEntityException;
import Persistencia.exceptions.PreexistingEntityException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;

/**
 *
 * @author Antonio
 */
@Entity
public class Sucursal implements Serializable {

    public static ControladoraPersistencia getPersistencia() {
        return persistencia;
    }
    @Id
    private int nroSucursal;
    @Basic
    private String cuil;
    @Basic
    private String telefono;
    @Basic
    private String razonSocial;
    private Deposito unDeposito;
    private String direccion;
    private ArrayList<Persona> listaPersonas;
    private ArrayList<Caja> listaCajas;
    private List<ListaDePrecio> ListaDePrecios= new LinkedList(); 
    private List<Empleado> listaEmpleados = new LinkedList();
    private List<Cliente> listaClientes = new LinkedList();
    private List<Proveedor> listaProveedores = new LinkedList();
    private List<DescripcionArticulo> listaDescripcionArticulo = new LinkedList();
    private List<Deposito> listaDeposito = new LinkedList();
    //private List<Gondola> listaGondola = new LinkedList();
    private Map<Integer,Gondola> listaGondola = new HashMap();
    private Map<Integer,Transferencia> listaTransferencia = new HashMap();
    private Map<Integer,RegistroVenta> listaRegistroVenta = new HashMap();
    private Map<Integer,Comprobante> listaComprobante = new HashMap();
    private Map<Integer,Compra> listaCompra = new HashMap();
    private Map<Integer,RegistroOrdenCompra> listaOrdenCompra = new HashMap();
    private Map<Integer,RegistroFacturaCompra> listaFacturaCompra = new HashMap();
    
    private static final ControladoraPersistencia persistencia = new ControladoraPersistencia();
//    private Object listaFacturaCompra; ESTO GENERABA EL ERROR PUT 
   
//    constructor con parámetros
    public Sucursal(int nroSucursal, String cuil, String telefono, String razonSocial) {
        this();
        this.nroSucursal = nroSucursal;
        this.cuil = cuil;
        this.telefono = telefono;
        this.razonSocial = razonSocial;
        this.unDeposito = unDeposito;
        this.direccion = direccion;
        this.listaPersonas = listaPersonas;
        this.listaCajas = listaCajas;
        this.ListaDePrecios = ListaDePrecios;
        this.listaProveedores = new LinkedList();
        this.listaClientes = new LinkedList();
        this.listaDescripcionArticulo = new LinkedList();
        this.listaDeposito = new LinkedList();
        this.listaEmpleados = new LinkedList();
        this.listaGondola = new HashMap();
        this.listaTransferencia = new HashMap();
        this.listaRegistroVenta = new HashMap();
        this.listaComprobante = new HashMap();
        this.listaCompra = new HashMap();
        this.listaOrdenCompra = new HashMap();
        this.listaFacturaCompra = new HashMap();
    }
    //    constructor nulo
    public Sucursal(){}
    
    public int getNroSucursal() {
        return nroSucursal;
    }

    public void setNroSucursal(int nroSucursal) {
        this.nroSucursal = nroSucursal;
    }

    public String getCuil() {
        return cuil;
    }

    public void setCuil(String cuil) {
        this.cuil = cuil;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public Deposito getUnDeposito() {
        return unDeposito;
    }

    public void setUnDeposito(Deposito unDeposito) {
        this.unDeposito = unDeposito;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public ArrayList<Persona> getListaPersonas() {
        return listaPersonas;
    }

    public void setListaPersonas(ArrayList<Persona> listaPersonas) {
        this.listaPersonas = listaPersonas;
    }

    public ArrayList<Caja> getListaCajas() {
        return listaCajas;
    }

    public void setListaCajas(ArrayList<Caja> listaCajas) {
        this.listaCajas = listaCajas;
    }

    public List<Empleado> getListaEmpleados() {
        return listaEmpleados;
    }

    public void setListaEmpleados(List<Empleado> listaEmpleados) {
        this.listaEmpleados = listaEmpleados;
    }
    public List<Empleado> cargarListaEmpleadosBD() {
        return this.listaEmpleados = Sucursal.persistencia.BuscarListaEmpleadoPersis();
    }
    public List<Proveedor> cargarListaProveedoresBD(){
        return this.listaProveedores = Sucursal.persistencia.BuscarListaProveedoresPersis();
    }
    public List<DescripcionArticulo> cargarListaDescripcionArticuloBD(){
        return this.listaDescripcionArticulo = Sucursal.persistencia.BuscarListaDescripcionArticuloPersis();
    }

    
    //EMPLEADO
    public void ModificarUnEmpleado(Empleado unEmpleado, String idPersona, String apellido, String nombre, String fechaNac, String sexo, String estadoCivil, String fechaIngreso, String cargo, String telefono, String direccion, String localidad, String provincia) throws PreexistingEntityException, Exception{
        unEmpleado.setIdPersona(idPersona);
        unEmpleado.setApellido(apellido);
        unEmpleado.setNombre(nombre);
        unEmpleado.setFechaNac(fechaNac);
        unEmpleado.setSexo(sexo);
        unEmpleado.setEstadoCivil(estadoCivil);
        unEmpleado.setFechaIngreso(fechaIngreso);
        unEmpleado.setCargo(cargo);
        unEmpleado.setTelefono(telefono);
        unEmpleado.setDireccion(direccion);
        unEmpleado.setProvincia(provincia);
        unEmpleado.setLocalidad(localidad);
        Sucursal.persistencia.ModificarUnEmpleadoPersis(unEmpleado);
    }    
    public void AgregarUnEmpleado(String idPersona, String apellido, String nombre, String fechaNac, String sexo, String estadoCivil, String fechaIngreso, String cargo, String telefono, String direccion, String localidad, String provincia) throws PreexistingEntityException, Exception{
	Empleado unEmpleado = new Empleado(idPersona, apellido, nombre, fechaNac, sexo, estadoCivil, fechaIngreso, cargo, telefono, direccion, localidad, provincia);
	this.listaEmpleados.add(unEmpleado);
	Sucursal.persistencia.AgregarUnEmpleadoPersis(unEmpleado);    
    }
    public Empleado BuscarEmpleado(String idPersona){
        Empleado aux=null, emp;
        Iterator it = this.cargarListaEmpleadosBD().iterator();
        while(it.hasNext()){
            emp=(Empleado) it.next();
            if(emp.getIdPersona().equals(idPersona)){
                aux=emp;
            }
        }
        return aux;
    }
    public void BorrarEmpleado(String idPersona, String apellido, String nombre, String fechaNac, String sexo, String estadoCivil, String fechaIngreso, String cargo, String telefono, String direccion, String localidad, String provincia) throws PreexistingEntityException, Exception{
     Empleado unEmpleado = new Empleado(idPersona, apellido, nombre, fechaNac, sexo, estadoCivil, fechaIngreso, cargo, telefono, direccion, localidad, provincia);
     this.listaEmpleados.remove(unEmpleado);
    Sucursal.persistencia.BorrarEmpleadoPersis(unEmpleado.getIdPersona());
    }

    ////////////////////////////////////////////////////////////////////////////////////
    
    //CLIENTE
    public void ModificarUnCliente(Cliente unCliente, String idPersona, String apellido, String nombre, String razonSocial, String tipoCliente, String ivaCondicion, String telefono, String direccion, String localidad, String provincia) throws PreexistingEntityException, Exception{
        unCliente.setIdPersona(idPersona);
        unCliente.setApellido(apellido);
        unCliente.setNombre(nombre);
        unCliente.setRazonSocial(razonSocial);
        unCliente.setTipoCliente(tipoCliente);
        unCliente.setIvaCondicion(ivaCondicion);
        unCliente.setTelefono(telefono);
        unCliente.setDireccion(direccion);
        unCliente.setProvincia(provincia);
        unCliente.setLocalidad(localidad);
        Sucursal.persistencia.ModificarClientePersis(unCliente);
    }
    public void AgregarUnCliente(String idPersona, String apellido, String nombre, String razonSocial, String tipoCliente, String ivaCondicion, String telefono, String direccion, String localidad, String provincia) throws PreexistingEntityException, Exception{
        Cliente unCliente = new Cliente(idPersona, apellido, nombre, razonSocial, tipoCliente, ivaCondicion, telefono, direccion, localidad, provincia);
        this.listaClientes.add(unCliente);
        Sucursal.persistencia.AgregarClientePersis(unCliente);
    }
    public Cliente BuscarCliente(String idPersona){
        Cliente aux=null, cli;
        Iterator it = this.cargarListaClientesBD().iterator();
        while(it.hasNext()){
            cli=(Cliente) it.next();
            if(cli.getIdPersona().equals(idPersona)){
                aux=cli;
            }
        }
        return aux;
    }
    public void BorrarCliente(String idPersona, String apellido, String nombre, String razonSocial, String tipoCliente, String ivaCondicion, String telefono, String direccion, String localidad, String provincia) throws PreexistingEntityException, Exception{
     Cliente unCliente = new Cliente(idPersona, apellido, nombre, razonSocial, tipoCliente, ivaCondicion, telefono, direccion, localidad, provincia);
     this.listaClientes.remove(unCliente);
    Sucursal.persistencia.BorrarClientePersis(unCliente.getIdPersona());
    }

    public List<Cliente> getListaClientes() {
        return listaClientes;
    }

    public void setListaClientes(List<Cliente> listaClientes) {
        this.listaClientes = listaClientes;
    }
    
    public List<Cliente> cargarListaClientesBD(){
        return this.listaClientes = Sucursal.persistencia.BuscarListaClientesPersis();
    }
    
    
    ////////////////////////////////////////////////////////////////////////////////////
    
    //PROVEEDOR
    public void ModificarUnProveedor(Proveedor unProveedor, String idPersona, String razonSocial, String situacionTributaria, String tipoProveduria, String telefono, String direccion, String localidad, String provincia)  throws PreexistingEntityException, Exception{
        unProveedor.setIdPersona(idPersona);
        unProveedor.setRazonSocial(razonSocial);
        unProveedor.setSituacionTributaria(situacionTributaria);
        unProveedor.setTipoProveduria(tipoProveduria);
        unProveedor.setTelefono(telefono);
        unProveedor.setDireccion(direccion);
        unProveedor.setLocalidad(localidad);
        unProveedor.setProvincia(provincia);
        Sucursal.persistencia.ModificarProveedorPersis(unProveedor);
    }
    
    public void AgregarUnProveedor(String idPersona, String razonSocial, String situacionTributaria, String tipoProveduria, String telefono, String direccion, String localidad, String provincia) throws PreexistingEntityException, Exception{
        Proveedor unProveedor = new Proveedor(idPersona, razonSocial, situacionTributaria, tipoProveduria, telefono, direccion, localidad, provincia);
        this.listaProveedores.add(unProveedor);
        Sucursal.persistencia.AgregarProveedorPersis(unProveedor);
    }
    
    public Proveedor BuscarProveedor(String idPersona){
        Proveedor aux=null, pro;
        Iterator it = this.cargarListaProveedoresBD().iterator();
        while(it.hasNext()){
            pro=(Proveedor) it.next();
            if(pro.getIdPersona().equals(idPersona)){
                aux=pro;
            }
        }
        return aux;
    }
    public void BorrarProveedor(String idPersona, String razonSocial, String situacionTributaria, String tipoProveduria, String telefono, String direccion, String localidad, String provincia) throws PreexistingEntityException, Exception{
     Proveedor unProveedor = new Proveedor(idPersona, razonSocial, situacionTributaria, tipoProveduria, telefono, direccion, localidad, provincia);
     this.listaProveedores.remove(unProveedor);
    Sucursal.persistencia.BorrarProveedorPersis(unProveedor.getIdPersona());
    }
    public List<Proveedor> getListaProveedores() {
        return listaProveedores;
    }

    public void setListaProveedores(List<Proveedor> listaProveedores) {
        this.listaProveedores = listaProveedores;
    }
    //DESCRIPCION ARTICULO////////////////////////////////////////////////
    public void ModificarUnaDescripcionArticulo(DescripcionArticulo unaDescripcionArticulo, String codigoBarra, String nombreArticulo, String descripcion, String tipoEnvase, String unidadMedida, float cantidadUnidadMedida, float precioCompra, float precioVenta, float precioVentaMay, int stockActual, int cantMinMay, int unidadesPorBulto) throws PreexistingEntityException, Exception { 
        unaDescripcionArticulo.setCodigoBarra(codigoBarra);
        unaDescripcionArticulo.setNombreArticulo(nombreArticulo);
        unaDescripcionArticulo.setDescripcion(descripcion);
        unaDescripcionArticulo.setTipoEnvase(tipoEnvase);
        unaDescripcionArticulo.setUnidadMedida(unidadMedida);
        unaDescripcionArticulo.setCantidadUnidadMedida(cantidadUnidadMedida);
        unaDescripcionArticulo.setPrecioCompra(precioCompra);
        unaDescripcionArticulo.setPrecioVenta(precioVenta);
        unaDescripcionArticulo.setPrecioVentaMay(precioVentaMay);
        unaDescripcionArticulo.setStockActual(stockActual);
        unaDescripcionArticulo.setCantMinMay(cantMinMay);
        unaDescripcionArticulo.setUnidadesPorBulto(nroSucursal);
        Sucursal.persistencia.ModificarUnaDescripcionArticuloPersis(unaDescripcionArticulo);    
    }
    
    public void AgregarUnaDescripcionArticulo(String codigoBarra, String nombreArticulo, String descripcion, String tipoEnvase, String unidadMedida, float cantidadUnidadMedida, float precioCompra, float precioVenta, float precioVentaMay, int stockActual, int cantMinMay, int unidadesPorBulto) throws PreexistingEntityException, Exception { 
        DescripcionArticulo unaDescripcionArticulo = new DescripcionArticulo(codigoBarra, nombreArticulo, descripcion, tipoEnvase, unidadMedida, cantidadUnidadMedida, precioCompra, precioVenta, precioVentaMay, stockActual, cantMinMay, unidadesPorBulto);
        this.listaDescripcionArticulo.add(unaDescripcionArticulo);
        Sucursal.persistencia.AgregarUnaDescripcionArticuloPersis(unaDescripcionArticulo);
    }
    
    public DescripcionArticulo BuscarDescripcionArticulo(String codigoBarra){
        DescripcionArticulo aux=null, descArt;
        Iterator it = this.cargarListaDescripcionArticuloBD().iterator();
        while(it.hasNext()){
            descArt=(DescripcionArticulo) it.next();
            if(descArt.getCodigoBarra().equals(codigoBarra)){
                aux=descArt;
            }
        }
        return aux;
    }

    public List<DescripcionArticulo> getListaDescripcionArticulo() {
        return listaDescripcionArticulo;
    }

    public void setListaDescripcionArticulo(List<DescripcionArticulo> listaDescripcionArticulo) {
        this.listaDescripcionArticulo = listaDescripcionArticulo;
    }
    /////////////////////////////////////////////////////////////////////////////
    ///DEPOSITO/////////////////////////////////////////////////////////////////
    public void ModificarDeposito(Deposito unDeposito, int nroDeposito, String codigoBarra, int stockReposicion, int stockActualDeposito, String proveedor) throws Exception{
        unDeposito.setCodigoBarra(codigoBarra);
        unDeposito.setStockReposicion(stockReposicion);
        unDeposito.setStockActualDeposito(stockActualDeposito);
        unDeposito.setProveedor(proveedor);
        Sucursal.persistencia.ModificarUnDepositoPersis(unDeposito);
    }
    public void AgregarUnDeposito(int nroDeposito, String codigoBarra, int stockReposicion, int stockActualDeposito, String proveedor) throws PreexistingEntityException, Exception { 
        Deposito unDeposito = new Deposito(nroDeposito, codigoBarra, stockReposicion, stockActualDeposito, proveedor);
        this.listaDeposito.add(unDeposito);
        Sucursal.persistencia.AgregarUnDepositoPersis(unDeposito);
    }
    public Deposito BuscarDeposito(String codigoBarra){
        Deposito aux=null, unDep;
        Iterator it = this.cargarListaDepositosBD().iterator();
        while(it.hasNext()){
            unDep=(Deposito) it.next();
            if(unDep.getCodigoBarra().equals(codigoBarra)){
                aux=unDep;
            }
        }
        return aux;
    }
    
    public List<Deposito> cargarListaDepositosBD(){
        return this.listaDeposito = Sucursal.persistencia.BuscarListaDepositoPersis();
    }

    public List<Deposito> getListaDeposito() {
        return listaDeposito;
    }

    public void setListaDeposito(List<Deposito> listaDeposito) {
        this.listaDeposito = listaDeposito;
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////GONDOLA////////////////////////////////////////////////////////////////////
    public void ModificarGondola(Gondola unaGondola, int nroGondola, String tipoGondola, String codigoBarra, int stockMinimo, int stockActualGondola, String sectorGondola) throws Exception{
        unaGondola.setNroGondola(nroGondola);
        unaGondola.setTipoGondola(tipoGondola);
        unaGondola.setCodigoBarra(codigoBarra);
        unaGondola.setStockActualGondola(stockActualGondola);
        unaGondola.setSectorGondola(sectorGondola);
        Sucursal.persistencia.ModificarGondolaPersis(unaGondola);
    }
    public void AgregarUnaGondola(int nroGondola, String tipoGondola, String codigoBarra, int stockMinimo, int stockActualGondola, String sectorGondola)throws PreexistingEntityException, Exception{
        Gondola unaGondola = new Gondola(nroGondola, tipoGondola, codigoBarra, stockMinimo, stockActualGondola, sectorGondola);
        this.listaGondola.put(unaGondola.getNroGondola(),unaGondola);
        Sucursal.persistencia.AgregarUnaGondolaPersis(unaGondola);
    }
      public Gondola BuscarGondola(String codigoBarra){
        Gondola aux=null, unaGon;
        Iterator it = this.cargarListaGondolaBD().values().iterator();
        while(it.hasNext()){
            unaGon=(Gondola) it.next();
            if(unaGon.getCodigoBarra().equals(codigoBarra)){
                aux=unaGon;
            }
        }
        return aux;
    }
    public Map<Integer,Gondola> cargarListaGondolaBD(){
        Iterator it= Sucursal.persistencia.BuscarListaGondolaPersis().iterator();
        Gondola unaGondola;
        while(it.hasNext()){
            unaGondola=(Gondola) it.next();
            listaGondola.put(unaGondola.getNroGondola(), unaGondola);
        }
        return listaGondola;
    }
    public int UltimoIdGondola(){
        int num=0;
        Gondola unaGon;
        Iterator it = this.cargarListaGondolaBD().values().iterator();
        while(it.hasNext()){
            unaGon = (Gondola) it.next();
            num=(num < unaGon.getNroGondola()) ? unaGon.getNroGondola() : num;
        }
        return num + 1;
    }
    public Map<Integer, Gondola> getListaGondola() {
        return listaGondola;
    }

    public void setListaGondola(Map<Integer, Gondola> listaGondola) {
        this.listaGondola = listaGondola;
    }
    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////TRANSFERENCIA////////////////////////////////////////////////////////////////////
    public void AgregarUnaTranferencia(int idTranferencia, String tipotransferencia, String origen, String destino, String fecha, String hora, int nroRemitoTranferencia) throws PreexistingEntityException, Exception{
        Transferencia unaTransferencia = new Transferencia(idTranferencia, tipotransferencia, origen, destino, fecha, hora, nroRemitoTranferencia);
        this.listaTransferencia.put(unaTransferencia.getIdTranferencia(), unaTransferencia);
        Sucursal.persistencia.AgregarUnaTransferenciaPersis(unaTransferencia);
    }
    public Transferencia BuscarTransferencia(int nroRemitoTransferencia){
        Transferencia aux=null, unaTrans;
        Iterator it = this.cargarListaTransferenciaBD().values().iterator();
        while(it.hasNext()){
            unaTrans=(Transferencia) it.next();
            if(unaTrans.getNroRemitoTranferencia() == (nroRemitoTransferencia)){
                aux=unaTrans;
            }
        }
        return aux;
    }
    public Map<Integer,Transferencia> cargarListaTransferenciaBD(){
        Iterator it= Sucursal.persistencia.BuscarListaTransferenciaPersis().iterator();
        Transferencia unaTransferencia;
        while(it.hasNext()){
            unaTransferencia=(Transferencia) it.next();
            listaTransferencia.put(unaTransferencia.getIdTranferencia(), unaTransferencia);
        }
        return listaTransferencia;
    }
    public int UltimoIdTransferencia(){
        int num=0;
        Transferencia unaTrans;
        Iterator it = this.cargarListaTransferenciaBD().values().iterator();
        while(it.hasNext()){
            unaTrans = (Transferencia) it.next();
            num=(num < unaTrans.getIdTranferencia()) ? unaTrans.getIdTranferencia() : num;
        }
        return num + 1;
    }

    public Map<Integer, Transferencia> getListaTransferencia() {
        return listaTransferencia;
    }

    public void setListaTransferencia(Map<Integer, Transferencia> listaTransferencia) {
        this.listaTransferencia = listaTransferencia;
    }
    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////REGISTRO VENTA////////////////////////////////////////////////////////////////////
    public void AgregarUnRegistroVenta(int idVenta, String codigoBarra, float precioVta, int cantidad, float costoCompra, Date fechaVenta, int nroComprobante) throws PreexistingEntityException, Exception{
        RegistroVenta unRegistroVenta = new RegistroVenta(idVenta, codigoBarra, precioVta, cantidad, costoCompra, fechaVenta, nroComprobante);
        this.listaRegistroVenta.put(unRegistroVenta.getIdVenta(), unRegistroVenta);
        Sucursal.persistencia.AgregarUnRegistroVentaPersis(unRegistroVenta);
    }
    
    public RegistroVenta BuscarRegistroVenta(int idVenta){
        RegistroVenta aux=null, unRegVta;
        Iterator it = this.cargarListaRegistroVentaBD().values().iterator();
        while(it.hasNext()){
            unRegVta=(RegistroVenta) it.next();
            if(unRegVta.getIdVenta()== (idVenta)){
                aux=unRegVta;
            }
        }
        return aux;
    }
    public Map<Integer,RegistroVenta> cargarListaRegistroVentaBD(){
        Iterator it= Sucursal.persistencia.BuscarListaRegistroVentaPersis().iterator();
        RegistroVenta unRegistroVenta;
        while(it.hasNext()){
            unRegistroVenta=(RegistroVenta) it.next();
            this.listaRegistroVenta.put(unRegistroVenta.getIdVenta(), unRegistroVenta);
        }
        return listaRegistroVenta;
    }
    public int UltimoIdVenta(){
        int num=0;
        RegistroVenta unRegVta;
        Iterator it = this.cargarListaRegistroVentaBD().values().iterator();
        while(it.hasNext()){
            unRegVta = (RegistroVenta) it.next();
            num=(num < unRegVta.getIdVenta()) ? unRegVta.getIdVenta() : num;
        }
        return num + 1;
    }

    public Map<Integer,RegistroVenta> getListaRegistroVenta() {
        return listaRegistroVenta;
    }

    public void setListaRegistroVenta(Map<Integer,RegistroVenta> listaRegistroVenta) {
        this.listaRegistroVenta = listaRegistroVenta;
    }
    
    public float CalcularResultados(Date fechaInicio, Date fechaFin){
        RegistroVenta unRegVta;
        float acumulador=0;
        float costoCompra;
        float precioVenta;
        int cantidad;
        float resultadoFila;
        Iterator it = this.cargarListaRegistroVentaBD().values().iterator();
        while(it.hasNext()){
            unRegVta = (RegistroVenta) it.next();
                      
            if( ((unRegVta.getFechaVenta().compareTo(fechaInicio)==1) || (unRegVta.getFechaVenta().compareTo(fechaFin)==0)) &&((unRegVta.getFechaVenta().compareTo(fechaFin)==-1) || unRegVta.getFechaVenta().compareTo(fechaFin)==0)){
                costoCompra = unRegVta.getCostoCompra();
                precioVenta = unRegVta.getPrecioVta();
                cantidad = unRegVta.getCantidad();
                resultadoFila = (precioVenta - costoCompra) * cantidad;
                acumulador = acumulador + resultadoFila;
            }
        }
        return acumulador;
    }
    public int cantidadResultados(Date fechaInicio, Date fechaFin){
        RegistroVenta unRegVta;
        int cant=0;
        Iterator it = this.cargarListaRegistroVentaBD().values().iterator();
        while(it.hasNext()){
            unRegVta = (RegistroVenta) it.next();          
            if( ((unRegVta.getFechaVenta().compareTo(fechaInicio)==1) || (unRegVta.getFechaVenta().compareTo(fechaFin)==0)) &&((unRegVta.getFechaVenta().compareTo(fechaFin)==-1) || unRegVta.getFechaVenta().compareTo(fechaFin)==0)){
                cant++;
            }
        }
        return cant;
    }
    
     public float TotalVentasPeriodo(Date fechaInicio, Date fechaFin){
        RegistroVenta unRegVta;
        float acumulador=0;
        float precioVenta;
        int cantidad;
        float resultadoFila;
        Iterator it = this.cargarListaRegistroVentaBD().values().iterator();
        while(it.hasNext()){
            unRegVta = (RegistroVenta) it.next();
            /*
            -1: Fecha1 < Fecha2
            0: Fecha1 == Fecha2
            1: Fecha1 > Fecha2            
            */            
            if( ((unRegVta.getFechaVenta().compareTo(fechaInicio)==1) || (unRegVta.getFechaVenta().compareTo(fechaFin)==0)) &&((unRegVta.getFechaVenta().compareTo(fechaFin)==-1) || unRegVta.getFechaVenta().compareTo(fechaFin)==0)){
                precioVenta = unRegVta.getPrecioVta();
                cantidad = unRegVta.getCantidad();
                resultadoFila = precioVenta * cantidad;
                acumulador = acumulador + resultadoFila;
            }
        }
        return acumulador;
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////
    /////////// COMPROBANTES////////////////////////////////////////////////////////////////////
    public void AgregarUnComprobante(int nroComprobante, String tipoComprobante, String copia, Date fecha, String idCliente, float totalComprobante, String puntoVenta) throws PreexistingEntityException, Exception{
        Comprobante unComprobante = new Comprobante(nroComprobante, tipoComprobante, copia, fecha, idCliente, totalComprobante, puntoVenta);
        this.listaComprobante.put(unComprobante.getNroComprobante(), unComprobante);
        Sucursal.persistencia.AgregarUnComprobantePersis(unComprobante);
    }
    public Comprobante BuscarComprobante(int nroComprobante){
        Comprobante aux=null, unComp;
        Iterator it = this.cargarListaComprobanteBD().values().iterator();
        while(it.hasNext()){
            unComp=(Comprobante) it.next();
            if(unComp.getNroComprobante()== (nroComprobante)){
                aux=unComp;
            }
        }
        return aux;
    }
    
    public Map<Integer,Comprobante> cargarListaComprobanteBD(){
        Iterator it= Sucursal.persistencia.BuscarListaComprobantePersis().iterator();
        Comprobante unComp;
        while(it.hasNext()){
            unComp=(Comprobante) it.next();
            this.listaComprobante.put(unComp.getNroComprobante(), unComp);
        }
        return listaComprobante;
    }
    
     public int UltimoNroComprobante(){
        int num=0;
        Comprobante unComp;
        Iterator it = this.cargarListaComprobanteBD().values().iterator();
        while(it.hasNext()){
            unComp = (Comprobante) it.next();
            num=(num < unComp.getNroComprobante()) ? unComp.getNroComprobante() : num;
        }
        return num + 1;
    }

    public Map<Integer,Comprobante> getListaComprobante() {
        return listaComprobante;
    }

    public void setListaComprobante(Map<Integer,Comprobante> listaComprobante) {
        this.listaComprobante = listaComprobante;
    }
    //////////////////////////////////////////////////////////////////////////////////////
    /////////// COMPRAS////////////////////////////////////////////////////////////////////
    public void AgregarUnaCompra(int nroCompra, Date fechaPedido, Date fechaEntrega, String terminosDeEntrega, String direccionEntrega, String autorizadoPor, int unaOrdenCompra) throws PreexistingEntityException, Exception{
        Compra unaCompra = new Compra(nroCompra, fechaPedido, fechaEntrega, terminosDeEntrega, direccionEntrega, autorizadoPor, unaOrdenCompra);
        this.listaCompra.put(unaCompra.getNroCompra(), unaCompra);
        Sucursal.persistencia.AgregarCompraPersis(unaCompra);
    }
    
    public Compra BuscarCompra(int nroCompra){
        Compra aux=null, unaCompra;
        Iterator it = this.cargarListaCompraBD().values().iterator();
        while(it.hasNext()){
            unaCompra=(Compra) it.next();
            if(unaCompra.getNroCompra()== (nroCompra)){
                aux=unaCompra;
            }
        }
        return aux;
    }
    
     public Map<Integer,Compra> cargarListaCompraBD(){
        Iterator it= Sucursal.persistencia.BuscarListaCompraPersis().iterator();
        Compra unaCompra;
        while(it.hasNext()){
            unaCompra=(Compra) it.next();
            this.listaCompra.put(unaCompra.getNroCompra(), unaCompra);
        }
        return listaCompra;
    }
     
     public int UltimoNroCompra(){
        int num=0;
        Compra unaCompra;
        Iterator it = this.cargarListaCompraBD().values().iterator();
        while(it.hasNext()){
            unaCompra = (Compra) it.next();
            num=(num < unaCompra.getNroCompra()) ? unaCompra.getNroCompra() : num;
        }
        return num + 1;
    }

    public Map<Integer,Compra> getListaCompra() {
        return listaCompra;
    }

    public void setListaCompra(Map<Integer,Compra> listaCompra) {
        this.listaCompra = listaCompra;
    }
     
    /////////////////////////////////////////////////////////////////////////////////////
    /////////// Registro de Compras////////////////////////////////////////////////////////////////////
    
    public void AgregarUnRegOrdenCompra(int idDetalleOrdenCompra, String codigoBarra, String articulo, int cantidad, float costoUnitario, float subtotalFila, String proveedor, Date fechaPedido) throws PreexistingEntityException, Exception{
        RegistroOrdenCompra unRegOrdenCompra = new RegistroOrdenCompra(idDetalleOrdenCompra, codigoBarra, articulo, cantidad, costoUnitario, subtotalFila, proveedor, fechaPedido);
        this.listaOrdenCompra.put(unRegOrdenCompra.getIdDetalleOrdenCompra(), unRegOrdenCompra);
        Sucursal.persistencia.AgregarRegistroOrdenCompraPersis(unRegOrdenCompra);
    }
    public RegistroOrdenCompra BuscarOrdenCompra(int idDetalleOrdenCompra){
        RegistroOrdenCompra aux=null, unRegOrdenCompra;
        Iterator it = this.cargarListaOrdenCompra().values().iterator();
        while(it.hasNext()){
            unRegOrdenCompra=(RegistroOrdenCompra) it.next();
            if(unRegOrdenCompra.getIdDetalleOrdenCompra() == (idDetalleOrdenCompra)){
                aux=unRegOrdenCompra;
            }
        }
        return aux;
    }
    public Map<Integer,RegistroOrdenCompra> cargarListaOrdenCompra(){
        Iterator it= Sucursal.persistencia.BuscarListaRegistroCompraPersis().iterator();
        RegistroOrdenCompra unRegOrdenCompra;
        while(it.hasNext()){
            unRegOrdenCompra=(RegistroOrdenCompra) it.next();
            this.listaOrdenCompra.put(unRegOrdenCompra.getIdDetalleOrdenCompra(), unRegOrdenCompra);
        }
        return listaOrdenCompra;
    }
    
    public int UltimoIdDetalleOrdenCompra(){
        int num=0;
        RegistroOrdenCompra unRegOrdenCompra;
        Iterator it = this.cargarListaOrdenCompra().values().iterator();
        while(it.hasNext()){
            unRegOrdenCompra = (RegistroOrdenCompra) it.next();
            num=(num < unRegOrdenCompra.getIdDetalleOrdenCompra()) ? unRegOrdenCompra.getIdDetalleOrdenCompra() : num;
        }
        return num + 1;
    }

    public Map<Integer,RegistroOrdenCompra> getListaOrdenCompra() {
        return listaOrdenCompra;
    }

    public void setListaOrdenCompra(Map<Integer,RegistroOrdenCompra> listaOrdenCompra) {
        this.listaOrdenCompra = listaOrdenCompra;
    }
    
    public void AgregarUnRegistroFactura(int idNroFacturaCompra, int nroProveedor, String razonSocial, String condicionIva, Date fechaVenta, float totalVenta, String TipoVenta) throws PreexistingEntityException, Exception{
       
         RegistroFacturaCompra unRegFacturaCompra = new RegistroFacturaCompra(idNroFacturaCompra, nroProveedor, razonSocial, condicionIva, fechaVenta, totalVenta, TipoVenta);
       
         this.listaFacturaCompra.put(unRegFacturaCompra.getIdNroFacturaCompra(), unRegFacturaCompra);
        Sucursal.persistencia.AgregarRegistroFacturaCompraPersis(unRegFacturaCompra);
    }

    public List<ListaDePrecio> getListaDePrecios() {
        return ListaDePrecios;
    }

    public Object getListaFacturaCompra() {
        return listaFacturaCompra;
    }
    
    
    
    
    
    
    
    
    
}
