/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;

/**
 *
 * @author Daniel
 */
@Entity
public class Compra implements Serializable{
    @Id
    private int nroCompra;
    @Basic
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date fechaPedido;
    @Basic
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date fechaEntrega; // Inicializado en null...
    @Basic
    private String terminosDeEntrega;
    @Basic
    private String direccionEntrega;
    @Basic
    private String autorizadoPor;
    @Basic
    private int unaOrdenCompra;
    
    
    public Compra(){}

    public Compra(int nroCompra, Date fechaPedido, Date fechaEntrega, String terminosDeEntrega, String direccionEntrega, String autorizadoPor, int unaOrdenCompra) {
        this.nroCompra = nroCompra;
        this.fechaPedido = fechaPedido;
        this.fechaEntrega = fechaEntrega;
        this.terminosDeEntrega = terminosDeEntrega;
        this.direccionEntrega = direccionEntrega;
        this.autorizadoPor = autorizadoPor;
        this.unaOrdenCompra = unaOrdenCompra;
    }

    public int getNroCompra() {
        return nroCompra;
    }

    public void setNroCompra(int nroCompra) {
        this.nroCompra = nroCompra;
    }

    public Date getFechaPedido() {
        return fechaPedido;
    }

    public void setFechaPedido(Date fechaPedido) {
        this.fechaPedido = fechaPedido;
    }

    public Date getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(Date fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public String getTerminosDeEntrega() {
        return terminosDeEntrega;
    }

    public void setTerminosDeEntrega(String terminosDeEntrega) {
        this.terminosDeEntrega = terminosDeEntrega;
    }

    public String getDireccionEntrega() {
        return direccionEntrega;
    }

    public void setDireccionEntrega(String direccionEntrega) {
        this.direccionEntrega = direccionEntrega;
    }

    public String getAutorizadoPor() {
        return autorizadoPor;
    }

    public void setAutorizadoPor(String autorizadoPor) {
        this.autorizadoPor = autorizadoPor;
    }

    public int getUnaOrdenCompra() {
        return unaOrdenCompra;
    }

    public void setUnaOrdenCompra(int unaOrdenCompra) {
        this.unaOrdenCompra = unaOrdenCompra;
    }

    
    
}
    