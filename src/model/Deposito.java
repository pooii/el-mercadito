/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author Antonio
 */
@Entity
public class Deposito implements Serializable {
    @Id
    private String codigoBarra;
    @Basic
    private int nroDeposito;
    @Basic
    private int stockReposicion;
    @Basic
    private int stockActualDeposito;
    @Basic
    private String proveedor;
    
    public Deposito(){}

    public Deposito(int nroDeposito, String codigoBarra, int stockReposicion, int stockActualDeposito, String proveedor) {
        this.nroDeposito = nroDeposito;
        this.codigoBarra = codigoBarra;
        this.stockReposicion = stockReposicion;
        this.stockActualDeposito = stockActualDeposito;
        this.proveedor = proveedor;
    }

    public int getNroDeposito() {
        return nroDeposito;
    }

    public void setNroDeposito(int nroDeposito) {
        this.nroDeposito = nroDeposito;
    }

    public String getCodigoBarra() {
        return codigoBarra;
    }

    public void setCodigoBarra(String codigoBarra) {
        this.codigoBarra = codigoBarra;
    }

    public int getStockReposicion() {
        return stockReposicion;
    }

    public void setStockReposicion(int stockReposicion) {
        this.stockReposicion = stockReposicion;
    }

    public int getStockActualDeposito() {
        return stockActualDeposito;
    }

    public void setStockActualDeposito(int stockActualDeposito) {
        this.stockActualDeposito = stockActualDeposito;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }
}