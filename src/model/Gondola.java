/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.io.Serializable;
import java.util.ArrayList;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
/**
 *
 * @author Antonio
 */
@Entity
public class Gondola implements Serializable{
    @Id
    private int nroGondola;
    @Basic
    private String tipoGondola;
    @Basic
    private String codigoBarra;
    @Basic
    private int stockMinimo;
    @Basic 
    private int stockActualGondola;
    @Basic
    private String sectorGondola;

    
    public Gondola(){} 

    public Gondola(int nroGondola, String tipoGondola, String codigoBarra, int stockMinimo, int stockActualGondola, String sectorGondola) {
        this.nroGondola = nroGondola;
        this.tipoGondola = tipoGondola;
        this.codigoBarra = codigoBarra;
        this.stockMinimo = stockMinimo;
        this.stockActualGondola = stockActualGondola;
        this.sectorGondola = sectorGondola;
    }

    public int getStockActualGondola() {
        return stockActualGondola;
    }

    public void setStockActualGondola(int stockActualGondola) {
        this.stockActualGondola = stockActualGondola;
    }

    public int getNroGondola() {
        return nroGondola;
    }

    public void setNroGondola(int nroGondola) {
        this.nroGondola = nroGondola;
    }

    public String getTipoGondola() {
        return tipoGondola;
    }

    public void setTipoGondola(String tipoGondola) {
        this.tipoGondola = tipoGondola;
    }

    public String getCodigoBarra() {
        return codigoBarra;
    }

    public void setCodigoBarra(String codigoBarra) {
        this.codigoBarra = codigoBarra;
    }

    public int getStockMinimo() {
        return stockMinimo;
    }

    public void setStockMinimo(int stockMinimo) {
        this.stockMinimo = stockMinimo;
    }

    public String getSectorGondola() {
        return sectorGondola;
    }

    public void setSectorGondola(String sectorGondola) {
        this.sectorGondola = sectorGondola;
    }
    
   
    
}
