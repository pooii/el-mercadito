/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;

/**
 *
 * @author Antonio
 */
@Entity
public class Comprobante implements Serializable {
    @Id
    private int nroComprobante;
    @Basic
    private String tipoComprobante;
    @Basic
    private String copia;
    @Basic
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date fecha;
    @Basic
    private String idCliente;
    @Basic
    private float totalComprobante;
    @Basic
    private String puntoVenta;
    
    public Comprobante(){}            

    public Comprobante(int nroComprobante, String tipoComprobante, String copia, Date fecha, String idCliente, float totalComprobante, String puntoVenta) {
        this.nroComprobante = nroComprobante;
        this.tipoComprobante = tipoComprobante;
        this.copia = copia;
        this.fecha = fecha;
        this.idCliente = idCliente;
        this.totalComprobante = totalComprobante;
        this.puntoVenta = puntoVenta;
    }

    public int getNroComprobante() {
        return nroComprobante;
    }

    public void setNroComprobante(int nroComprobante) {
        this.nroComprobante = nroComprobante;
    }

    public String getTipoComprobante() {
        return tipoComprobante;
    }

    public void setTipoComprobante(String tipoComprobante) {
        this.tipoComprobante = tipoComprobante;
    }

    public String getCopia() {
        return copia;
    }

    public void setCopia(String copia) {
        this.copia = copia;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public float getTotalComprobante() {
        return totalComprobante;
    }

    public void setTotalComprobante(float totalComprobante) {
        this.totalComprobante = totalComprobante;
    }

    public String getPuntoVenta() {
        return puntoVenta;
    }

    public void setPuntoVenta(String puntoVenta) {
        this.puntoVenta = puntoVenta;
    }
    
    
}
