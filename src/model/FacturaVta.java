package model;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
public class FacturaVta extends Comprobante implements Serializable{
    @Basic
   private String tipoFactura; 
   @Basic
   private int puntoVenta;
   @Basic
   private String fechaVto;
   @Basic
   private String fechaEmision;
   @OneToOne
   private Cliente unCliente;
   @Basic
   private float totales;

    public FacturaVta() {
    }

    public FacturaVta(String tipoFactura, int puntoVenta, String fechaVto, String fechaEmision, Cliente unCliente, float totales) {
        this.tipoFactura = tipoFactura;
        this.puntoVenta = puntoVenta;
        this.fechaVto = fechaVto;
        this.fechaEmision = fechaEmision;
        this.unCliente = unCliente;
        this.totales = totales;
    }

    public String getTipoFactura() {
        return tipoFactura;
    }

    public void setTipoFactura(String tipoFactura) {
        this.tipoFactura = tipoFactura;
    }


    public void setPuntoVenta(int puntoVenta) {
        this.puntoVenta = puntoVenta;
    }

    public String getFechaVto() {
        return fechaVto;
    }

    public void setFechaVto(String fechaVto) {
        this.fechaVto = fechaVto;
    }

    public String getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(String fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public Cliente getUnCliente() {
        return unCliente;
    }

    public void setUnCliente(Cliente unCliente) {
        this.unCliente = unCliente;
    }

    public float getTotales() {
        return totales;
    }

    public void setTotales(float totales) {
        this.totales = totales;
    }
   
   
 
}
