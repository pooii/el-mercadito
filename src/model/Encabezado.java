package model;

import java.util.logging.Logger;
import javax.persistence.Id;

/**
 *
 * @author Antonio
 */
public class Encabezado {
 private String nombreEmpresa="El Mercadito";
 private String situacionTributaria="Responsable Inscripto";
 private String cuit="20-27800277-3";
 private String ib="303030";
 private String inicioAct="01/03/2000";

    public Encabezado(){}
    
    public Encabezado(String nombreEmpresa, String situacionTributaria, String cuit, String ib) {
        this.nombreEmpresa = nombreEmpresa;
        this.situacionTributaria = situacionTributaria;
        this.cuit = cuit;
        this.ib = ib;
    }

    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    public String getSituacionTributaria() {
        return situacionTributaria;
    }

    public void setSituacionTributaria(String situacionTributaria) {
        this.situacionTributaria = situacionTributaria;
    }

    public String getCuit() {
        return cuit;
    }

    public void setCuit(String cuit) {
        this.cuit = cuit;
    }

    public String getIb() {
        return ib;
    }

    public void setIb(String ib) {
        this.ib = ib;
    }

    public String getInicioAct() {
        return inicioAct;
    }

    public void setInicioAct(String inicioAct) {
        this.inicioAct = inicioAct;
    }
    
    
}
