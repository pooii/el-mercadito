/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;

/**
 *
 * @author daniel
 */
@Entity
public class RegistroOrdenCompra implements Serializable {
    @Id
    private int idDetalleOrdenCompra;
    @Basic
    private String codigoBarra;
    @Basic 
    private String articulo;
    @Basic
    private int cantidad;
    @Basic
    private float costoUnitario;
    @Basic
    private float subtotalFila;
    @Basic
    private String proveedor;
    @Basic
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date fechaPedido;

    public RegistroOrdenCompra() {
    }

    public RegistroOrdenCompra(int idDetalleOrdenCompra, String codigoBarra, String articulo, int cantidad, float costoUnitario, float subtotalFila, String proveedor, Date fechaPedido) {
        this.idDetalleOrdenCompra = idDetalleOrdenCompra;
        this.codigoBarra = codigoBarra;
        this.articulo = articulo;
        this.cantidad = cantidad;
        this.costoUnitario = costoUnitario;
        this.subtotalFila = subtotalFila;
        this.proveedor = proveedor;
        this.fechaPedido = fechaPedido;
    }

    public int getIdDetalleOrdenCompra() {
        return idDetalleOrdenCompra;
    }

    public void setIdDetalleOrdenCompra(int idDetalleOrdenCompra) {
        this.idDetalleOrdenCompra = idDetalleOrdenCompra;
    }

    public String getCodigoBarra() {
        return codigoBarra;
    }

    public void setCodigoBarra(String codigoBarra) {
        this.codigoBarra = codigoBarra;
    }

    public String getArticulo() {
        return articulo;
    }

    public void setArticulo(String articulo) {
        this.articulo = articulo;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public float getCostoUnitario() {
        return costoUnitario;
    }

    public void setCostoUnitario(float costoUnitario) {
        this.costoUnitario = costoUnitario;
    }

    public float getSubtotalFila() {
        return subtotalFila;
    }

    public void setSubtotalFila(float subtotalFila) {
        this.subtotalFila = subtotalFila;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public Date getFechaPedido() {
        return fechaPedido;
    }

    public void setFechaPedido(Date fechaPedido) {
        this.fechaPedido = fechaPedido;
    }
    

    
    
}
