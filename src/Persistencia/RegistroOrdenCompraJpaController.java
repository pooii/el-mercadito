/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Persistencia;

import Persistencia.exceptions.NonexistentEntityException;
import Persistencia.exceptions.PreexistingEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import model.RegistroOrdenCompra;

/**
 *
 * @author Antonio
 */
public class RegistroOrdenCompraJpaController implements Serializable {

    public RegistroOrdenCompraJpaController() {
        this.emf = Persistence.createEntityManagerFactory("El_mercaditoPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(RegistroOrdenCompra registroOrdenCompra) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(registroOrdenCompra);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findRegistroOrdenCompra(registroOrdenCompra.getIdDetalleOrdenCompra()) != null) {
                throw new PreexistingEntityException("RegistroOrdenCompra " + registroOrdenCompra + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(RegistroOrdenCompra registroOrdenCompra) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            registroOrdenCompra = em.merge(registroOrdenCompra);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                int id = registroOrdenCompra.getIdDetalleOrdenCompra();
                if (findRegistroOrdenCompra(id) == null) {
                    throw new NonexistentEntityException("The registroOrdenCompra with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(int id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            RegistroOrdenCompra registroOrdenCompra;
            try {
                registroOrdenCompra = em.getReference(RegistroOrdenCompra.class, id);
                registroOrdenCompra.getIdDetalleOrdenCompra();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The registroOrdenCompra with id " + id + " no longer exists.", enfe);
            }
            em.remove(registroOrdenCompra);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<RegistroOrdenCompra> findRegistroOrdenCompraEntities() {
        return findRegistroOrdenCompraEntities(true, -1, -1);
    }

    public List<RegistroOrdenCompra> findRegistroOrdenCompraEntities(int maxResults, int firstResult) {
        return findRegistroOrdenCompraEntities(false, maxResults, firstResult);
    }

    private List<RegistroOrdenCompra> findRegistroOrdenCompraEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(RegistroOrdenCompra.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public RegistroOrdenCompra findRegistroOrdenCompra(int id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(RegistroOrdenCompra.class, id);
        } finally {
            em.close();
        }
    }

    public int getRegistroOrdenCompraCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<RegistroOrdenCompra> rt = cq.from(RegistroOrdenCompra.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
