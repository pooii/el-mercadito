/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistencia;
import java.util.ArrayList;
import ElMercadito.ElMercadito;
import model.Articulo;
import model.Caja;
import model.Cajero;
import model.Cliente;
import model.Compra;
import model.Comprobante;
import model.Deposito;
import model.DescripcionArticulo;
import model.Empleado;
import model.Encabezado;
import model.Encargado;
import model.FacturaVta;
import model.Gondola;
//import model.OrdenCompra;
import model.Pago;
import model.Persona;
import model.Proveedor;
import model.RegistroOrdenCompra;
import model.Sucursal;
import model.Transferencia;
import model.Venta;
import Persistencia.exceptions.NonexistentEntityException;
import Persistencia.exceptions.PreexistingEntityException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.RegistroVenta;
import model.RegistroFacturaCompra;

// * @author daniel

public class ControladoraPersistencia {
    public ControladoraPersistencia() {
    }
    ArticuloJpaController articuloPersistencia;
    CajaJpaController cajaPersistencia = new CajaJpaController();
    ClienteJpaController clientePersistencia = new ClienteJpaController();
    CompraJpaController compraPersistencia=new CompraJpaController();
    ComprobanteJpaController comprobantePersistencia = new ComprobanteJpaController();
    DepositoJpaController depositoPersistencia = new DepositoJpaController();
    DescripcionArticuloJpaController descripcionArticuloPersistencia=new DescripcionArticuloJpaController();
    EmpleadoJpaController empleadoPersistencia = new EmpleadoJpaController();
    EncabezadoJpaController encabezadoPersistencia= new EncabezadoJpaController();
    GondolaJpaController gondolaPersistencia = new GondolaJpaController();OrdenCompraJpaController ordenCompraPersistencia = new OrdenCompraJpaController();
    PagoJpaController pagoPersistencia = new PagoJpaController();
    PersonaJpaController personaPersistencia = new PersonaJpaController();
    ProveedorJpaController proveedorPersistencia = new ProveedorJpaController();
    TransferenciaJpaController transferenciaPersistencia= new TransferenciaJpaController ();
    VentaJpaController ventaPersistencia = new VentaJpaController();
    RegistroVentaJpaController registroVentaPersistencia = new RegistroVentaJpaController();
    RegistroOrdenCompraJpaController registroOrdenCompraPersistencia = new RegistroOrdenCompraJpaController();
    RegistroFacturaCompraJpaController registroFacturaCompraPersistencia=new RegistroFacturaCompraJpaController();


//    //Articulo////
    public void AgregarArticuloPersis(Articulo art) throws PreexistingEntityException, Exception{
       this.articuloPersistencia.create(art);
    }
    public void ModificarArticuloPersis(Articulo art) throws NonexistentEntityException, Exception{
        this.articuloPersistencia.edit(art);
    }
    public void BorrarArticuloPersis(int id) throws NonexistentEntityException{
        this.articuloPersistencia.destroy(id);
    }
    public List<Articulo> BuscarListaArticulos(){
       return this.articuloPersistencia.findArticuloEntities();
    }
       
    //Caja////
    
    public void AgregarCajaPersis(Caja caja) throws PreexistingEntityException, Exception{
       this.cajaPersistencia.create(caja);
    }
    public void ModificarCajaPersis(Caja caja) throws NonexistentEntityException, Exception{
        this.cajaPersistencia.edit(caja);
    }
    public void BorrarCajaPersis(int id) throws NonexistentEntityException{
        this.cajaPersistencia.destroy(id);
    }
    public List<Caja> BuscarListaCajas(){
       return this.cajaPersistencia.findCajaEntities();
    }
    public Caja BuscarUnaCaja(int id){
        return this.cajaPersistencia.findCaja(id);
}
    

    //Cliente////  
    public void AgregarClientePersis(Cliente cliente) throws PreexistingEntityException, Exception{
       this.clientePersistencia.create(cliente);
    }
    public void ModificarClientePersis(Cliente cliente) throws NonexistentEntityException, Exception{
        this.clientePersistencia.edit(cliente);
    }
    public void BorrarClientePersis(String id) throws NonexistentEntityException{
        this.clientePersistencia.destroy(id);
    }
    public List<Cliente> BuscarListaClientesPersis(){
       return this.clientePersistencia.findClienteEntities();
    }
    public Object BuscarUnCliente(String idPersona){
        return this.clientePersistencia.findCliente(idPersona);
    }


 //Proveedor///   
//    
    public void AgregarProveedorPersis(Proveedor proveedor) throws PreexistingEntityException, Exception{
       this.proveedorPersistencia.create(proveedor);
    }
    public void ModificarProveedorPersis(Proveedor proveedor) throws NonexistentEntityException, Exception{
        this.proveedorPersistencia.edit(proveedor);
    }
    public void BorrarProveedorPersis(String id) throws NonexistentEntityException{
        this.proveedorPersistencia.destroy(id);
    }
    public List<Proveedor> BuscarListaProveedoresPersis(){
       return this.proveedorPersistencia.findProveedorEntities();
    }
    public Object BuscarUnProveedor(String id){
        return this.proveedorPersistencia.findProveedor(id);
    }
    //Persona///   
//    
    public void AgregarPersonaPersis(Persona persona) throws PreexistingEntityException, Exception{
       this.personaPersistencia.create(persona);
    }
    public void ModificarPersonaPersis(Persona persona) throws NonexistentEntityException, Exception{
        this.personaPersistencia.edit(persona);
    }
    public void BorrarPersonaPersis(int id) throws NonexistentEntityException{
        this.personaPersistencia.destroy(id);
    }
    public List<Persona> BuscarListaPersonas(){
       return this.personaPersistencia.findPersonaEntities();
    }
    public Object BuscarUnPersona(String id){
        return this.personaPersistencia.findPersona(id);
    }
    
    //Empleado
    public void AgregarUnEmpleadoPersis(Empleado emp) throws PreexistingEntityException, Exception{
       this.empleadoPersistencia.create(emp);
    }
    public void ModificarUnEmpleadoPersis(Empleado emp) throws NonexistentEntityException, Exception{
        this.empleadoPersistencia.edit(emp);
    }
    public void BorrarEmpleadoPersis(String idPersona) throws NonexistentEntityException{
        this.empleadoPersistencia.destroy(idPersona) ;
    }
    public Empleado BuscarEmpleadoPersis(String idPersona){
        return (Empleado) this.empleadoPersistencia.findEmpleado(idPersona);
    }
    public List<Empleado> BuscarListaEmpleadoPersis(){
        return this.empleadoPersistencia.findEmpleadoEntities();
    }
    ///////////////////////////////////////////////////////
    
    //Descripcion Articulo
    public void AgregarUnaDescripcionArticuloPersis(DescripcionArticulo DescArt) throws PreexistingEntityException, Exception{
        this.descripcionArticuloPersistencia.create(DescArt);
    }
    public void ModificarUnaDescripcionArticuloPersis(DescripcionArticulo DescArt) throws PreexistingEntityException, Exception{
        this.descripcionArticuloPersistencia.edit(DescArt);
    }
    public void BorrarDescripcionArticuloPersis(String codigoBarra) throws NonexistentEntityException{
        this.descripcionArticuloPersistencia.destroy(codigoBarra);
    }
    public DescripcionArticulo BuscarDescripcionArticuloPersis(String codigoBarra){
        return (DescripcionArticulo) this.descripcionArticuloPersistencia.findDescripcionArticulo(codigoBarra);
    }
    public List<DescripcionArticulo> BuscarListaDescripcionArticuloPersis(){
        return this.descripcionArticuloPersistencia.findDescripcionArticuloEntities();
    }
    ///////////////////////////////////////////////////////////
    ////////////DEPOSITO ////////////////////////////////////////////////
    public void ModificarUnDepositoPersis(Deposito unDeposito) throws PreexistingEntityException, Exception{
        this.depositoPersistencia.edit(unDeposito);
    }

    public void AgregarUnDepositoPersis(Deposito unDeposito) throws PreexistingEntityException, Exception { 
        this.depositoPersistencia.create(unDeposito);
    }

    public List<Deposito> BuscarListaDepositoPersis() {
        return this.depositoPersistencia.findDepositoEntities();
    }
    ////////////////////////////////////////////////////////////////////////////////
    //////////////////GONDOLA///////////////////////////////////
    public void ModificarGondolaPersis(Gondola unaGondola) throws Exception {
        this.gondolaPersistencia.edit(unaGondola);
    }
    public void AgregarUnaGondolaPersis(Gondola unaGondola) throws PreexistingEntityException, Exception{
        this.gondolaPersistencia.create(unaGondola);
    }
    public List<Gondola> BuscarListaGondolaPersis(){
        return this.gondolaPersistencia.findGondolaEntities();
    }
    ////////////////////////////////////////////////////////////////////////////////
    //////////////////TRANSFERENCIA///////////////////////////////////
    public void AgregarUnaTransferenciaPersis(Transferencia unaTransferencia) throws PreexistingEntityException, Exception{
        this.transferenciaPersistencia.create(unaTransferencia);
    }
    public List<Transferencia> BuscarListaTransferenciaPersis(){
        return this.transferenciaPersistencia.findTransferenciaEntities();
    }
    public void ModificarTransferenciaPersis(Transferencia unaTransferencia) throws Exception{
        this.transferenciaPersistencia.edit(unaTransferencia);
    }
       ////////////////////////////////////////////////////////////////////////////////
    //////////////////REGISTRO VENTA///////////////////////////////////
    public void AgregarUnRegistroVentaPersis(RegistroVenta unRegistroVenta) throws Exception {
        this.registroVentaPersistencia.create(unRegistroVenta);
    }
    public List<RegistroVenta> BuscarListaRegistroVentaPersis(){
        return this.registroVentaPersistencia.findRegistroVentaEntities();
    }
    ///////////////////////////////////////////////////////////////////////////////
    /////////////////COMPROBANTES///////////////////////////////////
    public void AgregarUnComprobantePersis(Comprobante unComprobante) throws Exception{
        this.comprobantePersistencia.create(unComprobante);
    }
    public List<Comprobante> BuscarListaComprobantePersis(){
        return this.comprobantePersistencia.findComprobanteEntities();
    }
    
    ///////////////////////////////////////////////////////////////////////////////
    /////////////////COMPROBANTES///////////////////////////////////
    public void AgregarCompraPersis(Compra unaCompra) throws Exception{
        this.compraPersistencia.create(unaCompra);
    }
    public List<Compra> BuscarListaCompraPersis(){
        return this.compraPersistencia.findCompraEntities();
    }
    ///////////////////////////////////////////////////////////////////////////////
    /////////////////RegistroOrdenCompra///////////////////////////////////
    
    public void AgregarRegistroOrdenCompraPersis (RegistroOrdenCompra unRegOrdenCompra) throws Exception{
        this.registroOrdenCompraPersistencia.create(unRegOrdenCompra);
    }
    public List<RegistroOrdenCompra> BuscarListaRegistroCompraPersis(){
        return this.registroOrdenCompraPersistencia.findRegistroOrdenCompraEntities();
    }
    public void RegistroFacturaCompraJpaPersis (RegistroFacturaCompra unRegistroFacturaCompra)throws Exception{
        this.registroFacturaCompraPersistencia.create(unRegistroFacturaCompra);
    }
    public List<RegistroFacturaCompra> BuscarListaRegistroFacturaCompraPersis(){
        return this.registroFacturaCompraPersistencia.findRegistroFacturaCompraEntities();
    }

    public void AgregarRegistroFacturaCompraPersis(RegistroFacturaCompra unRegFacturaCompra) {
        try {
            this.registroFacturaCompraPersistencia.create(unRegFacturaCompra);
        } catch (Exception ex) {
            Logger.getLogger(ControladoraPersistencia.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}