/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Persistencia;

import Persistencia.exceptions.NonexistentEntityException;
import Persistencia.exceptions.PreexistingEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import model.RegistroVenta;

/**
 *
 * @author Antonio
 */
public class RegistroVentaJpaController implements Serializable {

    public RegistroVentaJpaController() {
        this.emf = Persistence.createEntityManagerFactory("El_mercaditoPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(RegistroVenta registroVenta) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(registroVenta);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findRegistroVenta(registroVenta.getIdVenta()) != null) {
                throw new PreexistingEntityException("RegistroVenta " + registroVenta + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(RegistroVenta registroVenta) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            registroVenta = em.merge(registroVenta);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                int id = registroVenta.getIdVenta();
                if (findRegistroVenta(id) == null) {
                    throw new NonexistentEntityException("The registroVenta with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(int id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            RegistroVenta registroVenta;
            try {
                registroVenta = em.getReference(RegistroVenta.class, id);
                registroVenta.getIdVenta();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The registroVenta with id " + id + " no longer exists.", enfe);
            }
            em.remove(registroVenta);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<RegistroVenta> findRegistroVentaEntities() {
        return findRegistroVentaEntities(true, -1, -1);
    }

    public List<RegistroVenta> findRegistroVentaEntities(int maxResults, int firstResult) {
        return findRegistroVentaEntities(false, maxResults, firstResult);
    }

    private List<RegistroVenta> findRegistroVentaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(RegistroVenta.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public RegistroVenta findRegistroVenta(int id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(RegistroVenta.class, id);
        } finally {
            em.close();
        }
    }

    public int getRegistroVentaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<RegistroVenta> rt = cq.from(RegistroVenta.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
